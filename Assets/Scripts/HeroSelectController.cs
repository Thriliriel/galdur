﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HeroSelectController : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler {
    //hero
    public int hero;

    //menu controller
    private EditDeckController editDeckController;
    //hero text
    private Text heroesParentText;

    // Use this for initialization
    void Awake () {
        editDeckController = GameObject.Find("EditDeckController").GetComponent<EditDeckController>();
        heroesParentText = GameObject.Find("Heroes").transform.Find("HeroText").GetComponentInChildren<Text>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        SetHeroText();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //set this hero
        editDeckController.playerHero = hero;
        editDeckController.hero.GetComponent<Image>().sprite = Resources.Load<Sprite>(editDeckController.playerCollectionHeroes[hero]
            .GetName());

        editDeckController.ToggleHeroesParent(false);
    }
    
    //set hero text
    public void SetHeroText()
    {
        string newText = editDeckController.playerCollectionHeroes[hero].GetName() + "\n" +
            editDeckController.playerCollectionHeroes[hero].GetRace().GetName() + " - " 
            + editDeckController.playerCollectionHeroes[hero].GetVocation().GetName() + "\n\n";

        foreach (EffectClass ec in editDeckController.playerCollectionHeroes[hero].GetEffect())
        {
            newText += ec.GetEffect() + "\n\n";
        }

        heroesParentText.text = newText;
    }
}
