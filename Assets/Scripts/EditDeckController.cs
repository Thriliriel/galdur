﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EditDeckController : MonoBehaviour {
    //hero select prefab
    public GameObject heroSelectPF;
    //card deck prefab
    public GameObject cardDeckPF;
    //all possible vocations
    public List<VocationClass> allVocations;
    //all possible races
    public List<RaceClass> allRaces;
    //all possible cards
    public List<CardClass> allCards;
    //all possible heroes
    public List<HeroClass> allHeroes;
    //displacement (for heroes panel)
    public int heroesDisplacement;
    //displacement (for collection panel)
    public int cardsDisplacement;
    //player collection cards
    public List<CardClass> playerCollectionCards;
    //player collection heroes
    public List<HeroClass> playerCollectionHeroes;
    //player deck
    public List<int> playerDeck;
    //player hero
    public int playerHero;
    //heroes object select
    public GameObject heroesParent;
    //collection parent
    public GameObject collectionParent;
    //input search
    public GameObject inputSearch;
    //warning deck size
    public GameObject warningDeckSize;
    //card border material
    public Sprite cardBoard;

    //hero
    public GameObject hero;
    //deck
    public GameObject[] deck;

    public void GoToScene(string sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    public void ToggleHeroesParent(bool value)
    {
        heroesParent.SetActive(value);
    }

    private void Awake()
    {
        heroesParent.SetActive(false);
        //warningDeckSize.SetActive(false);
        
        playerDeck = new List<int>();

        //load the file info
        List<GameClass> loadedInfo;
        try
        {
            loadedInfo = SaveLoadClass.Load();
        }
        catch
        {
            loadedInfo = SaveLoadClass.Load();
        }

        //set the vocations
        allVocations = loadedInfo[0].allVocations;
        //set the units
        allRaces = loadedInfo[0].allRaces;
        //set the cards
        allCards = loadedInfo[0].allCards;
        //set the heroes
        allHeroes = loadedInfo[0].allHeroes;

        //load player collection
        List<CollectionClass> loadedCollection = SaveLoadClass.LoadCollection();
        playerCollectionHeroes = loadedCollection[0].heroes;
        playerCollectionCards = loadedCollection[0].GetCardsCollection();

        //load player deck
        List<DeckClass> loadedDeck = SaveLoadClass.LoadDeck();
        playerHero = loadedDeck[0].heroIndex;
        foreach (int index in loadedDeck[0].cardsIndexes)
        {
            playerDeck.Add(index);
        }

        //set the hero
        hero.GetComponent<Image>().sprite = Resources.Load<Sprite>(playerCollectionHeroes[playerHero].GetName());

        //set the cards of the deck
        for(int i = 0; i < deck.Length; i++)
        {
            deck[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Cards/"+playerCollectionCards[playerDeck[i]].GetName());
            deck[i].GetComponent<CardDeckController>().collectionIndex = playerDeck[i];
        }

        //set all heroes the player has
        Vector3 startingPosition = heroSelectPF.transform.position;
        for(int i = 0; i < playerCollectionHeroes.Count; i++)
        {
            GameObject newHeroCollection = Instantiate(heroSelectPF, heroesParent.transform);
            
            //set sprite
            newHeroCollection.GetComponent<Image>().sprite = Resources.Load<Sprite>(playerCollectionHeroes[i].GetName());

            //set hero
            newHeroCollection.GetComponent<HeroSelectController>().hero = i;

            //set position
            int displacementX, displacementY = 0;
            displacementX = i % 3;
            displacementY = i / 3;
            newHeroCollection.GetComponent<RectTransform>().anchoredPosition = new Vector3(
                startingPosition.x + (displacementX * heroesDisplacement), startingPosition.y - (displacementY * heroesDisplacement), 0);
        }

        startingPosition = cardDeckPF.transform.position;
        //set the player collection
        for (int i = 0; i < playerCollectionCards.Count; i++)
        {
            GameObject newCardCollection = Instantiate(cardDeckPF, collectionParent.transform);

            //set sprite
            newCardCollection.GetComponent<Image>().sprite = Resources.Load<Sprite>("Cards/" + playerCollectionCards[i].GetName());

            //set index
            newCardCollection.GetComponent<CardDeckController>().collectionIndex = i;

            //set position
            int displacementX, displacementY = 0;
            displacementX = i % 8;
            displacementY = i / 8;
            newCardCollection.GetComponent<RectTransform>().anchoredPosition = new Vector3(
                startingPosition.x + (displacementX * cardsDisplacement), startingPosition.y - (displacementY * (cardsDisplacement * 1.5f)), 0);
        }
    }

    private void Start()
    {
        //set the collection height, according qnt of cards / 8, * 100 for the height of each card + spacement
        int heightCollection = (((int)Mathf.Ceil(playerCollectionCards.Count / 8)) * 100) + 40;
        collectionParent.transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(
            collectionParent.transform.parent.gameObject.GetComponent<RectTransform>().sizeDelta.x, heightCollection);
    }

    //remove from deck
    public void RemoveCardFromDeck(int indexToRemove)
    {
        //find the card to remove material
        foreach (GameObject crd in deck)
        {
            if(crd.GetComponent<CardDeckController>().collectionIndex == indexToRemove)
            {
                crd.GetComponent<Image>().sprite = cardBoard;
                crd.GetComponent<CardDeckController>().collectionIndex = -1;
                playerDeck.Remove(indexToRemove);
                break;
            }
        }
    }

    //add to deck
    public void AddCardInDeck(int indexToAdd)
    {
        if (indexToAdd >= 0)
        {
            //find some card empty to put it in
            foreach (GameObject crd in deck)
            {
                if (crd.GetComponent<CardDeckController>().collectionIndex == -1)
                {
                    crd.GetComponent<Image>().sprite = Resources.Load<Sprite>("Cards/" + playerCollectionCards[indexToAdd].GetName());
                    crd.GetComponent<CardDeckController>().collectionIndex = indexToAdd;
                    playerDeck.Add(indexToAdd);
                    break;
                }
            }
        }
    }

    //save the new deck
    public void SaveDeck()
    {
        //first, check if the deck has the 12 cards
        int qntCards = 0;
        foreach(GameObject cd in deck)
        {
            if(cd.GetComponent<CardDeckController>().collectionIndex == -1)
            {
                qntCards = 0;
                break;
            }

            qntCards++;
        }

        //if deck has no 12 cards, cannot save
        if (qntCards < 12)
        {
            StartCoroutine(DeckSizeWarning(2));
        }
        else
        {
            //save all in the file
            DeckClass.current = new DeckClass(0, playerDeck, true);

            //hero (first for default)
            DeckClass.current.heroIndex = playerHero;

            //SAVE FILE
            SaveLoadClass.SaveDeck();

            //back to the menu
            GoToScene("menu");
        }
    }

    //filter the collection cards
    public void FilterCards()
    {
        //reset the cards inside collection panel
        for(int i = 0; i < collectionParent.transform.childCount; i++)
        {
            Destroy(collectionParent.transform.GetChild(i).gameObject);
        }

        string search = inputSearch.GetComponent<InputField>().text.ToLower();

        Vector3 startingPosition = cardDeckPF.transform.position;
        int countIndex = 0;
        //set the player collection
        for (int i = 0; i < playerCollectionCards.Count; i++)
        {
            //just instantiate if it is inside the search criteria
            //search for card name, card race, card vocation and card effect
            bool inst = false;

            if (playerCollectionCards[i].GetName().ToLower().Contains(search))
            {
                inst = true;
            }

            if (playerCollectionCards[i].GetRace().GetName().ToLower().Contains(search))
            {
                inst = true;
            }

            if (playerCollectionCards[i].GetVocation().GetName().ToLower().Contains(search))
            {
                inst = true;
            }

            if (playerCollectionCards[i].GetEffects() != null)
            {
                foreach (EffectClass ec in playerCollectionCards[i].GetEffects())
                {
                    if (ec.GetEffect().ToLower().Contains(search) || ec.GetGeneralEffect().ToLower().Contains(search))
                    {
                        inst = true;
                        break;
                    }
                }
            }

            if (inst)
            {
                GameObject newCardCollection = Instantiate(cardDeckPF, collectionParent.transform);

                //set sprite
                newCardCollection.GetComponent<Image>().sprite = Resources.Load<Sprite>("Cards/" + playerCollectionCards[i].GetName());

                //set index
                newCardCollection.GetComponent<CardDeckController>().collectionIndex = i;

                //set position
                int displacementX, displacementY = 0;
                displacementX = countIndex % 8;
                displacementY = countIndex / 8;
                newCardCollection.GetComponent<RectTransform>().anchoredPosition = new Vector3(
                    startingPosition.x + (displacementX * cardsDisplacement), startingPosition.y - 
                    (displacementY * (cardsDisplacement * 1.5f)), 0);

                countIndex++;
            }
        }
    }

    //deck size warning
    private IEnumerator DeckSizeWarning(float seconds)
    {
        Animator anim = warningDeckSize.GetComponent<Animator>();
        anim.SetBool("deckSizeWarning", true);
        yield return new WaitForSeconds(seconds);
        anim.SetBool("deckSizeWarning", false);
    }
}
