﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HeroTitleController : MonoBehaviour, IPointerClickHandler {
    private EditDeckController editDeckController;

    private void Awake()
    {
        editDeckController = GameObject.Find("EditDeckController").GetComponent<EditDeckController>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        editDeckController.ToggleHeroesParent(true);
    }
}
