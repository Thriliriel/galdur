﻿//class for cards effects

/*
Possible general effects:
0 - Status Change (condition, attack, defense, array against(array(race), array(class)), array ingame(array(race), array(class)), target).
1 - Mana Change (condition, value,  max/min, target).
2 - Life Change (condition, value, max/min, target).
3 - Steal Mana (condition, value, min).
4 - Disable Power (condition).
5 - Blank Status (condition).
6 - Initiative.
7 - Copy Status (condition).
8 - Copy Power (condition).
9 - Change Reinforcement (condition, power).
10 - Tribal Force (condition, power, race).

//future abilities
11 - Steal Life (condition, value, min). - already implemented, need to test
*/

using System.Collections.Generic;

[System.Serializable]
public class EffectClass {
    //constructor
    public EffectClass()
    {
        generalEffect = "nope";
    }

    public EffectClass(string newGeneralEffect, string newEffect, string newAttribute, string newCondition,
        List<string> newTarget, int newAttackModifier, int newDefenseModifier, int newValueModifier, int newMaxModifier,
        int newMinModifier, EffectClass newReinforcement, RaceClass newRace, List<RaceClass> newAgainstRace, 
        List<VocationClass> newAgainstVocation, List<RaceClass> newIngameRace, List<VocationClass> newIngameVocation)
    {
        generalEffect = newGeneralEffect;
        effect = newEffect;
        attribute = newAttribute;
        condition = newCondition;
        target = newTarget;
        attackModifier = newAttackModifier;
        defenseModifier = newDefenseModifier;
        valueModifier = newValueModifier;
        maxModifier = newMaxModifier;
        minModifier = newMinModifier;
        reinforcement = newReinforcement;
        race = newRace;
        againstRace = newAgainstRace;
        againstVocation = newAgainstVocation;
        ingameRace = newIngameRace;
        ingameVocation = newIngameVocation;
    }

    //general effect
    private string generalEffect;
    //effect
    private string effect;
    //attribute it changes
    private string attribute;
    //condition to trigger effect
    //possible conditions: Attack, Defend, Lose, Win, Turn
    private string condition;
    //target for the effect
    //possible targets: Self, Opponent, array(Classe), array(Raça)
    private List<string> target;
    //value to change attack
    private int attackModifier;
    //value to change defense
    private int defenseModifier;
    //value to change mana, life, etc
    private int valueModifier;
    //max value modifier allowed
    private int maxModifier;
    //min value modifier allowed
    private int minModifier;
    //effect to replace Reinforcement or be used for Tribal Force
    private EffectClass reinforcement;
    //race to be used for tribal force
    private RaceClass race;
    //races to be playing against to activate effect
    private List<RaceClass> againstRace = new List<RaceClass>();
    //vocations to be playing against to activate effect
    private List<VocationClass> againstVocation = new List<VocationClass>();
    //races which need to be in game to activate effect
    private List<RaceClass> ingameRace = new List<RaceClass>();
    //vocations which need to be in game to activate effect
    private List<VocationClass> ingameVocation = new List<VocationClass>();

    //Getters and Setters
    public string GetGeneralEffect()
    {
        return generalEffect;
    }
    public void SetGeneralEffect(string value)
    {
        generalEffect = value;
    }
    public string GetEffect()
    {
        return effect;
    }
    public void SetEffect(string value)
    {
        effect = value;
    }
    public string GetAttribute()
    {
        return attribute;
    }
    public void SetAttribute(string value)
    {
        attribute = value;
    }
    public string GetCondition()
    {
        return condition;
    }
    public void SetCondition(string value)
    {
        condition = value;
    }
    public List<string> GetTarget()
    {
        return target;
    }
    public void SetTarget(List<string> value)
    {
        target = value;
    }
    public int GetAttackModifier()
    {
        return attackModifier;
    }
    public void SetAttackModifier(int value)
    {
        attackModifier = value;
    }
    public int GetDefenseModifier()
    {
        return defenseModifier;
    }
    public void SetDefenseModifier(int value)
    {
        defenseModifier = value;
    }
    public int GetValueModifier()
    {
        return valueModifier;
    }
    public void SetValueModifier(int value)
    {
        valueModifier = value;
    }
    public int GetMaxModifier()
    {
        return maxModifier;
    }
    public void SetMaxModifier(int value)
    {
        maxModifier = value;
    }
    public int GetMinModifier()
    {
        return minModifier;
    }
    public void SetMinModifier(int value)
    {
        minModifier = value;
    }
    public EffectClass GetReinforcement()
    {
        return reinforcement;
    }
    public void SetReinforcement(EffectClass value)
    {
        reinforcement = value;
    }
    public RaceClass GetRace()
    {
        return race;
    }
    public void SetRace(RaceClass value)
    {
        race = value;
    }
    public List<RaceClass> GetAgainstRace()
    {
        return againstRace;
    }
    public void SetAgainstRace(List<RaceClass> value)
    {
        againstRace = value;
    }
    public List<VocationClass> GetAgainstVocation()
    {
        return againstVocation;
    }
    public void SetAgainstVocation(List<VocationClass> value)
    {
        againstVocation = value;
    }
    public List<RaceClass> GetIngameRace()
    {
        if (ingameRace == null)
        {
            return new List<RaceClass>();
        }
        else
        {
            return ingameRace;
        }
    }
    public void SetIngameRace(List<RaceClass> value)
    {
        ingameRace = value;
    }
    public List<VocationClass> GetIngameVocation()
    {
        if(ingameVocation == null)
        {
            return new List<VocationClass>();
        }
        else {
            return ingameVocation;
        }
    }
    public void SetIngameVocation(List<VocationClass> value)
    {
        ingameVocation = value;
    }
}
