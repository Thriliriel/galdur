﻿//class for heroes

//ALL HEROES:
/*
//0 - Trosh
//1 - Ahkson, The Black Shaman
//2 - Bolin, The Defender
//3 - Tolin, The Light Paladin
//4 - Galthoniel, The Bowsinger
//5 - Tumissa, The Lady of Forest
//6 - Baladir, The Swordmaster
//7 - Radir, The Shadow
*/

using System.Collections.Generic;

[System.Serializable]
public class HeroClass {
    //constructs
    public HeroClass()
    {
    }

	public HeroClass(string newName, RaceClass newRace, VocationClass newVocation, List<EffectClass> newEffect)
    {
        name = newName;
		race = newRace;
		vocation = newVocation;
        effect = newEffect;
    }

    //name
    private string name;
    //race
    private RaceClass race;
    //vocation
    private VocationClass vocation;
    //effects
    private List<EffectClass> effect;
    
    //Getters and Setters
    public string GetName()
    {
        return name;
    }
    public void SetName(string value)
    {
        name = value;
    }
	public RaceClass GetRace()
	{
		return race;
	}
	public void SetRace(RaceClass value)
	{
		race = value;
	}
	public VocationClass GetVocation()
	{
		return vocation;
	}
	public void SetVocation(VocationClass value)
	{
		vocation = value;
	}
    public List<EffectClass> GetEffect()
    {
        return effect;
    }
    public void SetEffect(List<EffectClass> value)
    {
        effect = value;
    }

    //find all effects with the same name
    public List<EffectClass> FindEffects(string value)
    {
        //list with all effects found
        List<EffectClass> effectsFound = new List<EffectClass>();

        //if not null
        if (effect != null)
        {
            if (effect.Count > 0)
            {
                foreach (EffectClass ec in effect)
                {
                    if (ec.GetGeneralEffect() == value)
                    {
                        effectsFound.Add(ec);
                    }
                }
            }
        }

        return effectsFound;
    }

    //check if the effect specified has the Race specified
    public bool CheckEffectRaceAgainst(EffectClass chosenEff, string raceName)
    {
        bool foundRace = false;

        if (chosenEff.GetAgainstRace() != null)
        {
            //if the other card is the same race, it is ok
            foreach (RaceClass rc in chosenEff.GetAgainstRace())
            {
                if (rc.GetName() == raceName)
                {
                    foundRace = true;
                    break;
                }
            }
        }
        else
        {
            //if there is no race condition, return true, since there is nothing preventing the effect to be applied
            foundRace = true;
        }

        return foundRace;
    }

    //check if the effect specified has the Race specified
    public bool CheckEffectVocationAgainst(EffectClass chosenEff, string vocationName)
    {
        bool foundVocation = false;

        if (chosenEff.GetAgainstVocation() != null)
        {
            //if the other card is the same vocation, it is ok
            foreach (VocationClass vc in chosenEff.GetAgainstVocation())
            {
                if (vc.GetName() == vocationName)
                {
                    foundVocation = true;
                    break;
                }
            }
        }
        else
        {
            //if there is no vocation condition, return true, since there is nothing preventing the effect to be applied
            foundVocation = true;
        }

        return foundVocation;
    }
}
