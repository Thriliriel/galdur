﻿using System.Collections.Generic;

//class to store player deck, in the game and in the bin files
[System.Serializable]
public class DeckClass {
    //current GameClass instance
    public static DeckClass current;
    //hero index
    public int heroIndex;
    //cards indexes
    public List<int> cardsIndexes;
    //is this deck equipped?
    public bool isEquipped;

    public DeckClass()
    {
        isEquipped = false;
        heroIndex = 0;
        cardsIndexes = new List<int>();
    }

    public DeckClass(int newHero, List<int> newCards, bool newIsEquipped)
    {
        isEquipped = newIsEquipped;
        heroIndex = newHero;
        cardsIndexes = newCards;
    }
}
