﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
   
    public void GoToScene(string sceneToLoad)
    {
        SceneManager.LoadScene(sceneToLoad);
    }
}
