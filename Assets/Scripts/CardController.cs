﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour {

    //big copy card
    private GameObject bigCard;
    //board
    private GameObject board;
    //game controller
    private GameController gameController;
    //board player 1
    private GameObject boardPlayer1;
    //is card played?
    private bool cardPlayed;
    //card class
    public CardClass cardClass;
    //original scale
    public Vector3 originalScale;
    //can see big card?
    public bool canSee;

    private void Awake()
    {
        //get te board game object
        board = GameObject.Find("Board");
        //hide the border
        transform.Find("CardBorder").GetComponent<SpriteRenderer>().enabled = false;
        //find the game controller
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
        //find the border
        boardPlayer1 = GameObject.Find("BoardPlayer1");
        //default
        cardPlayed = false;

        originalScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    private void OnMouseOver()
    {
        //create a copy of this gameObject, if it still does not exists and card not played yet and game is not over
        if (bigCard == null && !gameController.gameOver && canSee)//&& !cardPlayed 
        {
            if (!cardPlayed)
            {
                //show the border
                transform.Find("CardBorder").GetComponent<SpriteRenderer>().enabled = true;

                //play wupp!
                transform.Find("CardBorder").GetComponent<AudioSource>().Play();
            }

            //copy
            bigCard = Instantiate(gameObject);

            //remove the script component
            Destroy(bigCard.GetComponent<CardDeckController>());

            //position it at the center
            bigCard.transform.position = new Vector3(12, 3, 5);

            //reset rotation
            bigCard.transform.localRotation = Quaternion.Euler(60, 0, 0);

            //double scale
            bigCard.transform.localScale = new Vector3(0.7f, 0.7f, originalScale.z);
        }
    }

    private void OnMouseExit()
    {
        //if big copy exists
        if (bigCard != null)
        {
            //hide the border
            transform.Find("CardBorder").GetComponent<SpriteRenderer>().enabled = false;

            //deletes it
            Destroy(bigCard);
        }
    }

    private void OnMouseUp()
    {
        //just play if not changing turn and game is not over and card is not played yet
        if (!gameController.isChangingTurn && !gameController.gameOver && !cardPlayed)
        {
            //verifies if has any card with Initiative, which needs to be played first
            List<int> initiativeIndexes = new List<int>();
            for (int i = 0; i < gameController.GetPlayerHand().transform.childCount; i++)
            {
                //Debug.Log("IA - " +cardsPlayer2.transform.GetChild(i).GetComponent<CardController>().cardClass.GetName());
                EffectClass ec = gameController.GetPlayerHand().transform.GetChild(i).GetComponent<CardController>().cardClass.FindEffect("Initiative");
                if (ec.GetGeneralEffect() != "nope")
                {
                    initiativeIndexes.Add(i);
                }
            }

            //if has any
            if(initiativeIndexes.Count > 0)
            {
                //check if this card played has initiative. If so, let him play
                EffectClass ec = cardClass.FindEffect("Initiative");
                if (ec.GetGeneralEffect() != "nope")
                {
                    gameController.ShowManaChoice(gameObject);
                }//otherwise, you shall not pass!
                else
                {
                    StartCoroutine(gameController.InitiativeText(2));
                }
            }//else, it is ok to play whatever card he wants to
            else
            {
                gameController.ShowManaChoice(gameObject);
            }
        }
    }

    //play card
    public void PlayCard(int qntMana)
    {
        //card played
        cardPlayed = true;

        //find the card turn place
        GameObject place = boardPlayer1.transform.GetChild(gameController.turn - 1).gameObject;

        //play sound
        gameObject.GetComponent<AudioSource>().Play();

        //play the card
        transform.parent = place.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(90, 0, 0);
        transform.localScale = new Vector3(2.2f, 1.6f, 1);
    }

    //set card played
    public void SetCardPlayed(bool value)
    {
        cardPlayed = value;
    }
}
