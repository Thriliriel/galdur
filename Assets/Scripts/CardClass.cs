﻿//class for cards

//ALL CARDS:
/*
//ORCS!!!
//0 - Light Shaman
//1 - Bogus
//2 - Orcish Bow
//3 - Orc Soldier
//4 - The Crazy Shaman
//5 - The Kamikaze Orc
//6 - Warrior Orc
//7 - Orc Doctor
//8 - Xenophobic Orc
//9 - The Old Shaman
//10 - Thrall, The Orc Shield
//11 - Siege Leader
//12 - Mhhaaark
//13 - Mimic Orc
//14 - Frenetic Barbarian
//15 - The Reaper
//16 - Blomb, The Orc Leader
//17 - Groroth, The Elfbreaker
//18 - Hrash, The human-eater
//19 - Momor, The Ahkson Acolyte

//DWARVES!!!
//20 - Mine Dwarf
//21 - Dwarf Warden
//22 - Outlander
//23 - The Crafter
//24 - Dwarf Army
//25 - Dwarf Cleric
//26 - The Tiny Runaway
//27 - Foreman Dwarf
//28 - Lont Apprentice
//29 - The Orc hunter
//30 - The Flak Dwarf
//31 - Dwarf Jeweler
//32 - Buorlin, The Elf Friend
//33 - Muor, The Champion
//34 - Lont, The Canceller
//35 - Gili, The Protector
//36 - Fulin, The Peacemaker
//37 - Minara, The Old Lady
//38 - Murolin, The Wall Scout
//39 - Olvidin, The Golden King

//ELVES!!!
//40 - Elf Scout
//41 - Elf Warrior
//42 - Elvish Sage
//43 - Elvish Druid
//44 - Wood Ranger
//45 - Harmless Elf
//46 - Tellarius, Magebreaker
//47 - Dallius
//48 - Glannil
//49 - Thrillin, Orcbane
//50 - Acheron
//51 - Greenleaf
//52 - Zarallonis, Shieldbreaker
//53 - Singlinn, Shapeshifter
//54 - Glingant
//55 - Elite Archer
//56 - Hrigan
//57 - Danarglin
//58 - Thriliriel, Bane of Shadows
//59 - Sarillin, Beacon of Hope

//HUMANS!!!
//60 - Human Scout
//61 - Human Defender
//62 - Human Archer
//63 - Herbalist
//64 - Arcane
//65 - Crazy Man
//66 - Tricky
//67 - Frontline Soldier
//68 - Bardin
//69 - Dollus
//70 - An, The Ritualist
//71 - Marlisa
//72 - Drodt, Hammerbearer
//73 - Mirt, Plainhidden
//74 - Brativos
//75 - Gormund
//76 - Craniack, Weird Shaman
//77 - Gront, The Blockade
//78 - Brutus
//79 - Trinnis, the Copycat
*/

using System.Collections.Generic;

[System.Serializable]
public class CardClass {
    //constructs
    public CardClass()
    {
        effect = new List<EffectClass>();
    }

	public CardClass(string newName, string newRarity, RaceClass newRace, VocationClass newVocation, int newAttack,
		int newDefense, int newCost, List<EffectClass> newEffect)
    {
        effect = new List<EffectClass>();

        name = newName;
		rarity = newRarity;
		race = newRace;
		vocation = newVocation;
		attack = newAttack;
		defense = newDefense;
		cost = newCost;
        effect = newEffect;
    }

    //name
    private string name;
    //rarity
	//possible: common, uncommon, rare, unique
	private string rarity;
    //race
    private RaceClass race;
    //vocation
    private VocationClass vocation;
	//attack
	private int attack;
	//defense
	private int defense;
	//cost
	private int cost;
    //effects
    private List<EffectClass> effect;
    
    //start lists
    public void StartLists()
    {
        effect = new List<EffectClass>();
    }

    //Getters and Setters
    public string GetName()
    {
        return name;
    }
    public void SetName(string value)
    {
        name = value;
    }
	public string GetRarity()
	{
		return rarity;
	}
	public void SetRarity(string value)
	{
		rarity = value;
	}
	public RaceClass GetRace()
	{
		return race;
	}
	public void SetRace(RaceClass value)
	{
		race = value;
	}
	public VocationClass GetVocation()
	{
		return vocation;
	}
	public void SetVocation(VocationClass value)
	{
		vocation = value;
	}
	public int GetAttack()
	{
		return attack;
	}
	public void SetAttack(int value)
	{
		attack = value;
	}
	public int GetDefense()
	{
		return defense;
	}
	public void SetDefense(int value)
	{
		defense = value;
	}
	public int GetCost()
	{
		return cost;
	}
	public void SetCost(int value)
	{
		cost = value;
	}
    public List<EffectClass> GetEffects()
    {
        return effect;
    }
    public void SetEffects(List<EffectClass> value)
    {
        effect = value;
    }

    //find some effect by its name
    public EffectClass FindEffect(string value)
    {
        //if not null
        if (effect != null)
        {
            if (effect.Count > 0)
            {
                foreach (EffectClass ec in effect)
                {
                    if (ec.GetGeneralEffect() == value)
                    {
                        return ec;
                    }
                }

                return new EffectClass();
            }
            else
            {
                return new EffectClass();
            }
        }
        else
        {
            return new EffectClass();
        }
    }

    //find all effects with the same name
    public List<EffectClass> FindEffects(string value)
    {
        //list with all effects found
        List<EffectClass> effectsFound = new List<EffectClass>();

        //if not null
        if (effect != null)
        {
            if (effect.Count > 0)
            {
                foreach (EffectClass ec in effect)
                {
                    if (ec.GetGeneralEffect() == value)
                    {
                        effectsFound.Add(ec);
                    }
                }
            }
        }

        return effectsFound;
    }

    //check if the effect specified has the Race specified
    public bool CheckEffectRaceAgainst(EffectClass chosenEff, string raceName)
    {
        bool foundRace = false;

        if (chosenEff.GetAgainstRace() != null)
        {
            //if the other card is the same race, it is ok
            foreach (RaceClass rc in chosenEff.GetAgainstRace())
            {
                if (rc.GetName() == raceName)
                {
                    foundRace = true;
                    break;
                }
            }
        }
        else
        {
            //if there is no race condition, return true, since there is nothing preventing the effect to be applied
            foundRace = true;
        }

        return foundRace;
    }

    //check if the effect specified has the Race specified
    public bool CheckEffectVocationAgainst(EffectClass chosenEff, string vocationName)
    {
        bool foundVocation = false;

        if (chosenEff.GetAgainstVocation() != null)
        {
            //if the other card is the same vocation, it is ok
            foreach (VocationClass vc in chosenEff.GetAgainstVocation())
            {
                if (vc.GetName() == vocationName)
                {
                    foundVocation = true;
                    break;
                }
            }
        }
        else
        {
            //if there is no vocation condition, return true, since there is nothing preventing the effect to be applied
            foundVocation = true;
        }

        return foundVocation;
    }
}
