﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    //all possible vocations
    public List<VocationClass> allVocations;
    //all possible races
    public List<RaceClass> allRaces;
    //all possible cards
    public List<CardClass> allCards;
    //all possible heroes
    public List<HeroClass> allHeroes;
    //turn
    public int turn;
    //change turn effect
    public GameObject changeTurn;
    //is it changing turn?
    public bool isChangingTurn;
    //possible images for mana
    public Sprite manaOut;
    public Sprite manaIn;
    //prefab health
    public GameObject healthPF;
    //prefab mana
    public GameObject manaPF;
    //prefab mana effect
    public GameObject manaEffectPF;
    //card prefab
    public GameObject cardPF;
    //is game over?
    public bool gameOver;

    //player panel
    private GameObject playerPanel;
    //player hero panel
    private GameObject playerHeroPanel;
    //player battle panel
    private GameObject playerBattlePanel;
    //opponent battle panel
    private GameObject opponentBattlePanel;
    //opponent panel
    private GameObject opponentPanel;
    //mana bar choice
    private GameObject manaBarChoice;
    //game over
    private GameObject gameOverObj;
    //initiative text object
    private GameObject initiativeObj;
    //which card the player is playing
    private GameObject cardPlaying;
    //does IA plays first?
    private bool IAFirst;
    //menu button
    public GameObject menuButton;

    //just to have a value
    //@TODO: place it on player controller
    private int playerHealth;
    private int playerMana;
    private int opponentHealth;
    private int opponentMana;
    //combat panel
    private GameObject combatPanel;
    //player collection cards
    private List<CardClass> playerCollectionCards;
    //player collection heroes
    private List<HeroClass> playerCollectionHeroes;
    //player deck
    private List<CardClass> playerDeck;
    //player hero
    private HeroClass playerHero;
    //opponent deck
    private List<CardClass> opponentDeck;
    //opponent hero
    private HeroClass opponentHero;
    //Player hand
    private GameObject playerHand;
    //IA hand
    private GameObject IAHand;
    //player board
    private GameObject playerBoard;
    //opponent board
    private GameObject opponentBoard;
    //mana used from player in the turn
    private int manaUsed;
    //mana used from IA in the turn
    private int manaUsedIA;

    private void Awake()
    {
        turn = 1;
        isChangingTurn = false;
        playerHealth = opponentHealth = 10;
        playerMana = opponentMana = 10;
        combatPanel = GameObject.Find("BattlePanel");
        playerPanel = GameObject.Find("PlayerPanel");
        playerBattlePanel = GameObject.Find("PlayerBattleInfo");
        playerHeroPanel = GameObject.Find("PlayerHero");
        opponentPanel = GameObject.Find("OpponentPanel");
        opponentBattlePanel = GameObject.Find("OpponentBattleInfo");
        manaBarChoice = GameObject.Find("ManaBarChoice");
        gameOverObj = GameObject.Find("GameOver");
        initiativeObj = GameObject.Find("InitiativeText");
        playerDeck = new List<CardClass>();
        opponentDeck = new List<CardClass>();
        playerHand = GameObject.Find("CardsPlayer1");
        IAHand = GameObject.Find("CardsPlayer2");
        playerBoard = GameObject.Find("BoardPlayer1");
        opponentBoard = GameObject.Find("BoardPlayer2");
        manaUsed = manaUsedIA = 0;
        gameOver = false;

        //plays first
        IAFirst = (Random.Range(0, 100) % 2 == 0) ? true : false;

        //holy hell
        manaBarChoice.transform.parent.transform.parent.gameObject.SetActive(false);

        combatPanel.SetActive(false);

        gameOverObj.SetActive(false);
        initiativeObj.SetActive(false);

        //load the file info
        List<GameClass> loadedInfo;
        try
        {
            loadedInfo = SaveLoadClass.Load();
        }
        catch
        {
            SaveInitialDatabase();
            loadedInfo = SaveLoadClass.Load();
        }

        //set the vocations
        allVocations = loadedInfo[0].allVocations;
        //set the units
        allRaces = loadedInfo[0].allRaces;
        //set the cards
        allCards = loadedInfo[0].allCards;
        //set the heroes
        allHeroes = loadedInfo[0].allHeroes;

        //load player collection
        List<CollectionClass> loadedCollection;
        try
        {
            loadedCollection = SaveLoadClass.LoadCollection();
        }
        catch
        {
            //save the initial collection
            SaveCollection();
            loadedCollection = SaveLoadClass.LoadCollection();
        }

        playerCollectionHeroes = loadedCollection[0].heroes;
        playerCollectionCards = loadedCollection[0].GetCardsCollection();

        //load player deck
        List<DeckClass> loadedDeck;
        try
        {
            loadedDeck = SaveLoadClass.LoadDeck();
        }
        catch
        {
            //save the initial deck
            SaveDeck();
            loadedDeck = SaveLoadClass.LoadDeck();
        }

        playerHero = playerCollectionHeroes[loadedDeck[0].heroIndex];
        foreach(int index in loadedDeck[0].cardsIndexes)
        {
            playerDeck.Add(new CardClass(playerCollectionCards[index].GetName(), playerCollectionCards[index].GetRarity(),
                playerCollectionCards[index].GetRace(), playerCollectionCards[index].GetVocation(), 
                playerCollectionCards[index].GetAttack(), playerCollectionCards[index].GetDefense(),
                playerCollectionCards[index].GetCost(), playerCollectionCards[index].GetEffects()));
        }

        //load IA deck
        List<DeckClass> loadedIADeck;
        try
        {
            loadedIADeck = SaveLoadClass.LoadIADeck();
        }
        catch
        {
            //save the initial deck
            SaveIADeck();
            loadedIADeck = SaveLoadClass.LoadIADeck();
        }

        //opponent hero
        opponentHero = playerCollectionHeroes[loadedIADeck[0].heroIndex];
        foreach (int index in loadedIADeck[0].cardsIndexes)
        {
            opponentDeck.Add(new CardClass(playerCollectionCards[index].GetName(), playerCollectionCards[index].GetRarity(),
                playerCollectionCards[index].GetRace(), playerCollectionCards[index].GetVocation(),
                playerCollectionCards[index].GetAttack(), playerCollectionCards[index].GetDefense(),
                playerCollectionCards[index].GetCost(), playerCollectionCards[index].GetEffects()));
        }
    }

    // Use this for initialization
    void Start () {
		//place the health and mana on the bar
        //player panel
        for(int i = 0; i < playerPanel.transform.childCount; i++)
        {
            //get the child
            GameObject child = playerPanel.transform.GetChild(i).gameObject;

            //clear the children of this child
            for(int j = 0; j < child.transform.childCount; j++)
            {
                Destroy(child.transform.GetChild(j).gameObject);
            }

            //now, if its the LifeBar, place little hearths
            if(child.name == "LifeBar")
            {
                Vector3 initialPos = healthPF.GetComponent<RectTransform>().anchoredPosition;
                for(int j = 1; j <= playerHealth; j++)
                {
                    GameObject newHearth =  Instantiate(healthPF);
                    //parent
                    //newHearth.transform.parent = child.transform;
                    newHearth.transform.SetParent(child.transform);
                    //rect transform
                    newHearth.GetComponent<RectTransform>().anchoredPosition = initialPos;
                    newHearth.GetComponent<RectTransform>().localScale = healthPF.GetComponent<RectTransform>().localScale;
                    //name
                    newHearth.name = "Health_" + j;

                    //update pos
                    initialPos = new Vector3(initialPos.x+28, initialPos.y, initialPos.z);
                }
            }//else, if its the ManaBar, place little purple potions
            else if (child.name == "ManaBar")
            {
                Vector3 initialPos = manaPF.GetComponent<RectTransform>().anchoredPosition;
                for (int j = 1; j <= playerMana; j++)
                {
                    GameObject newMana = Instantiate(manaPF);
                    //parent
                    //newMana.transform.parent = child.transform;
                    newMana.transform.SetParent(child.transform);
                    //rect transform
                    newMana.GetComponent<RectTransform>().anchoredPosition = initialPos;
                    newMana.GetComponent<RectTransform>().localScale = manaPF.GetComponent<RectTransform>().localScale;
                    //name
                    newMana.name = "Mana_" + j;

                    //update pos
                    initialPos = new Vector3(initialPos.x + 28, initialPos.y, initialPos.z);
                }
            }
        }

        //opponent panel
        for (int i = 0; i < opponentPanel.transform.childCount; i++)
        {
            //get the child
            GameObject child = opponentPanel.transform.GetChild(i).gameObject;

            //clear the children of this child
            for (int j = 0; j < child.transform.childCount; j++)
            {
                Destroy(child.transform.GetChild(j).gameObject);
            }

            //now, if its the LifeBar, place little hearths
            if (child.name == "LifeBar")
            {
                Vector3 initialPos = healthPF.GetComponent<RectTransform>().anchoredPosition;
                for (int j = 1; j <= opponentHealth; j++)
                {
                    GameObject newHearth = Instantiate(healthPF);
                    //parent
                    //newHearth.transform.parent = child.transform;
                    newHearth.transform.SetParent(child.transform);
                    //rect transform
                    newHearth.GetComponent<RectTransform>().anchoredPosition = initialPos;
                    newHearth.GetComponent<RectTransform>().localScale = healthPF.GetComponent<RectTransform>().localScale;
                    //name
                    newHearth.name = "Health_" + j;

                    //update pos
                    initialPos = new Vector3(initialPos.x + 28, initialPos.y, initialPos.z);
                }
            }//else, if its the ManaBar, place little purple potions
            else if (child.name == "ManaBar")
            {
                Vector3 initialPos = manaPF.GetComponent<RectTransform>().anchoredPosition;
                for (int j = 1; j <= opponentMana; j++)
                {
                    GameObject newMana = Instantiate(manaPF);
                    //parent
                    //newMana.transform.parent = child.transform;
                    newMana.transform.SetParent(child.transform);
                    //rect transform
                    newMana.GetComponent<RectTransform>().anchoredPosition = initialPos;
                    newMana.GetComponent<RectTransform>().localScale = manaPF.GetComponent<RectTransform>().localScale;
                    //name
                    newMana.name = "Mana_" + j;

                    //update pos
                    initialPos = new Vector3(initialPos.x + 28, initialPos.y, initialPos.z);
                }
            }
        }

        //place the cards in the player hand, as well the hero in its place
        //each position + 2.25
        //select 4 random cards from player deck
        Vector3 initialPosition = cardPF.transform.position;
        List<int> indexesChosen = new List<int>();
        for(int i = 0; i < 4; i++)
        {
            //select 1 random
            int indexChosen = Random.Range(0, playerDeck.Count);

            //while the card was already chosen, try again
            while (indexesChosen.Contains(indexChosen))
            {
                indexChosen = Random.Range(0, playerDeck.Count);
            }

            //add it as chosen
            indexesChosen.Add(indexChosen);
            
            //instantiate
            GameObject newCard = Instantiate(cardPF, initialPosition, cardPF.transform.rotation);
            //change parent
            //newCard.transform.parent = playerHand.transform;
            newCard.transform.SetParent(playerHand.transform);
            //change name
            newCard.name = "Card" + (i + 1);
            //card class
            newCard.GetComponent<CardController>().cardClass = playerDeck[indexChosen];
            //card image
            newCard.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Cards/" + playerDeck[indexChosen].GetName());

            Debug.Log(newCard.GetComponent<CardController>().cardClass.GetName());

            //update position to next
            initialPosition = new Vector3(initialPosition.x + 2.25f, initialPosition.y, initialPosition.z);
        }
        //change parent info
        playerHand.transform.position = new Vector3(2.8f, 1.5f, 1.3f);
        playerHand.transform.rotation = Quaternion.Euler(-20, 0, 0);
        playerHand.transform.localScale = new Vector3(0.5f, 1, 0.5f);

        //hero
        playerHeroPanel.GetComponent<Image>().sprite = Resources.Load<Sprite>(playerHero.GetName());

        //choose IA card classes
        ChooseCardsIA();

        //IA CARDS
        for(int i = 0; i < IAHand.transform.childCount; i++)
        {
            GameObject oppCard = IAHand.transform.GetChild(i).gameObject;
            Debug.Log("IA - " + oppCard.GetComponent<CardController>().cardClass.GetName());
        }

        //if IA moves first, play!
        if (IAFirst)
        {
            IAMove();
        }
    }
	
	// Update is called once per frame
	void Update () {
        RaycastHit2D hit;
        Camera mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (hit = Physics2D.Raycast(ray.origin, new Vector2(0, 0)))
            Debug.Log(hit.collider.name);
    }

    //choose IA card classes
    private void ChooseCardsIA()
    {
        //choose the card classes for IA
        List<int> indexesChosen = new List<int>();
        for (int i = 0; i < 4; i++)
        {
            //select 1 random
            int indexChosen = Random.Range(0, opponentDeck.Count);

            //while the card was already chosen, try again
            while (indexesChosen.Contains(indexChosen))
            {
                indexChosen = Random.Range(0, opponentDeck.Count);
            }

            //add it as chosen
            indexesChosen.Add(indexChosen);
            
            //get the card
            GameObject oppCard = IAHand.transform.GetChild(i).gameObject;
            //card class
            oppCard.GetComponent<CardController>().cardClass = new CardClass(opponentDeck[indexChosen].GetName(),
                opponentDeck[indexChosen].GetRarity(), opponentDeck[indexChosen].GetRace(), opponentDeck[indexChosen].GetVocation(),
                opponentDeck[indexChosen].GetAttack(), opponentDeck[indexChosen].GetDefense(), opponentDeck[indexChosen].GetCost(),
                opponentDeck[indexChosen].GetEffects());
            //card image
            oppCard.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Cards/" + opponentDeck[indexChosen].GetName());
            //Debug.Log("IA - " + oppCard.GetComponent<CardController>().cardClass.GetName());
        }
    }

    //show the mana bar choice
    public void ShowManaChoice(GameObject cardPlayed)
    {
        //delete all inside
        for(int j = 0; j < manaBarChoice.transform.childCount; j++)
        {
            GameObject.Destroy(manaBarChoice.transform.GetChild(j).gameObject);
        }

        //set the card playing
        cardPlaying = cardPlayed;

        //place the card image
        manaBarChoice.transform.parent.transform.parent.Find("Card").GetComponent<Image>().sprite =
            cardPlayed.GetComponent<SpriteRenderer>().sprite;

        //place the qnt mana inside
        Vector3 initialPos = manaEffectPF.GetComponent<RectTransform>().anchoredPosition;
        for (int j = 1; j <= playerMana; j++)
        {
            GameObject newMana = Instantiate(manaEffectPF);
            //parent
            //newMana.transform.parent = manaBarChoice.transform;
            newMana.transform.SetParent(manaBarChoice.transform);
            //rect transform
            newMana.GetComponent<RectTransform>().anchoredPosition = initialPos;
            newMana.GetComponent<RectTransform>().localScale = manaEffectPF.GetComponent<RectTransform>().localScale;
            //name
            newMana.name = "ManaChoice_" + j;

            //two lines
            float yPos = initialPos.y;
            float xPos = initialPos.x;
            if (j == 10)
            {
                yPos = manaEffectPF.GetComponent<RectTransform>().anchoredPosition.y - 60;
                xPos = manaEffectPF.GetComponent<RectTransform>().anchoredPosition.x - 45;
            }

            //update pos
            initialPos = new Vector3(xPos + 45, yPos, initialPos.z);

            //Unity bug, need to do the local copy for the delegate get the right value
            int temp = j;

            //place the events
            //mouse over
            EventTrigger trigger = newMana.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
            entry.callback.AddListener((eventData) => { MouseOver(temp); });
            trigger.triggers.Add(entry);

            //mouse exit
            trigger = newMana.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerExit;
            entry.callback.AddListener((eventData) => { MouseExit(temp); });
            trigger.triggers.Add(entry);

            //mouse click
            trigger = newMana.GetComponent<EventTrigger>();
            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener((eventData) => { MouseClick(temp); });
            trigger.triggers.Add(entry);
        }

        //show it
        manaBarChoice.transform.parent.transform.parent.gameObject.SetActive(true);
    }

    //mouse over mana to select
    public void MouseOver(int mana)
    {
        //get all mana effect
        GameObject[] manaEffects = GameObject.FindGameObjectsWithTag("ManaEffect").OrderBy(go => go.name).ToArray();
        
        //reorder to make the numbers right (0 1 2 3, instead 0 1 10 11)
        System.Array.Sort(manaEffects, delegate (GameObject a, GameObject b)
        {
            string[] breakA = a.name.Split('_');
            string[] breakB = b.name.Split('_');
            int indA = System.Int32.Parse(breakA[breakA.Length - 1]);
            int indB = System.Int32.Parse(breakB[breakB.Length - 1]);
            return (indA).CompareTo(indB);
        });

        //foreach of them
        for (int i = 0; i < manaEffects.Length; i++)
        {
            //if its name is this one of before, mana in
            if(i+1 <= mana)
            {
                manaEffects[i].GetComponent<Image>().sprite = manaIn;
            }//otherwise, mana out
            else
            {
                manaEffects[i].GetComponent<Image>().sprite = manaOut;
            }
        }
    }

    //mouse out mana to select
    public void MouseExit(int mana)
    {
        //get all mana effect
        GameObject[] manaEffects = GameObject.FindGameObjectsWithTag("ManaEffect");

        //foreach of them
        foreach(GameObject manaEff in manaEffects)
        {
            //change the sprite
           manaEff.GetComponent<Image>().sprite = manaOut;
        }
    }

    //mouse click for mana select
    public void MouseClick(int mana)
    {
        //check if the qnt of mana chosen is enough to pay, at least, the card cost
        //although, if the qnt mana available is less than needed, let it play, but with penalty later
        //if not, show warning
        if (mana < cardPlaying.GetComponent<CardController>().cardClass.GetCost())
        {
            //test the total mana available with the card costs
            bool isReallyFucked = true;
            for(int i = 0; i < playerHand.transform.childCount; i++)
            {
                //if there is a card with a cost less or equal the available mana, he can actually play it
                if(playerHand.transform.GetChild(i).GetComponent<CardController>().cardClass.GetCost() <= playerMana)
                {
                    isReallyFucked = false;
                }
            }

            //if he really has no mana, let it play with penalty
            if (isReallyFucked)
            {
                PlayCard(mana);
            }//otherwise, he cannot play
            else
            {
                manaBarChoice.transform.parent.transform.parent.Find("ManaWarning").GetComponent<Animator>().SetBool("warnMana", true);
            }
        }//else, play normal
        else
        {
            //play the card
            PlayCard(mana);
        }
    }

    //hide the mana choice canvas
    public void HideManaChoice()
    {
        manaBarChoice.transform.parent.transform.parent.gameObject.SetActive(false);
    }

    //play the card (player)
    private void PlayCard(int mana)
    {
        //just to be sure
        manaBarChoice.transform.parent.transform.parent.Find("ManaWarning").GetComponent<Animator>().SetBool("warnMana", false);

        //hide mana choice
        HideManaChoice();

        //play the card
        cardPlaying.GetComponent<CardController>().PlayCard(mana);

        //nullifies card playing
        cardPlaying = null;

        //diminish the mana in the board
        ReduceMana(playerPanel, mana, 0);

        //set mana Used
        manaUsed = mana;

        //if IA move first, combat
        if (IAFirst)
        {
            Combat();
        }//else, IA moves
        else
        {
            //IA Move
            IAMove();
        }
    }

    //IA plays
    public void IAMove()
    {
        //verifies if has any card with Initiative, which needs to be played first
        List<int> initiativeIndexes = new List<int>();
        for(int i = 0; i < IAHand.transform.childCount; i++)
        {
            //Debug.Log("IA - " +cardsPlayer2.transform.GetChild(i).GetComponent<CardController>().cardClass.GetName());
            EffectClass ec = IAHand.transform.GetChild(i).GetComponent<CardController>().cardClass.FindEffect("Initiative");
            if (ec.GetGeneralEffect() != "nope")
            {
                initiativeIndexes.Add(i);
            }
        }

        GameObject randomCard;
        //if has any card with initiative, sort out one of them,
        if (initiativeIndexes.Count > 0)
        {
            randomCard = IAHand.transform.GetChild(initiativeIndexes[Random.Range(0, initiativeIndexes.Count)]).gameObject;
            Debug.Log(randomCard.GetComponent<CardController>().cardClass.GetName());
        }//else, sort out any
        else
        {
            randomCard = IAHand.transform.GetChild(Random.Range(0, IAHand.transform.childCount)).gameObject;
        }

        //find the border
        GameObject boardPlayer2 = GameObject.Find("BoardPlayer2");

        //card played
        randomCard.GetComponent<CardController>().SetCardPlayed(true);
        randomCard.GetComponent<CardController>().canSee = true;

        //find the card turn place
        GameObject place = boardPlayer2.transform.GetChild(turn - 1).gameObject;

        //play the card
        randomCard.transform.parent = place.transform;
        randomCard.transform.localPosition = Vector3.zero;
        randomCard.transform.localRotation = Quaternion.Euler(90, 0, 0);
        randomCard.transform.localScale = new Vector3(2.2f, 1.6f, 1);

        //select a random qnt of mana, plus the card quantity
        //basically, always use 2-4 mana
        manaUsedIA = Random.Range(2, 5);
        //just check if the mana used if not too much
        if(manaUsedIA > opponentMana)
        {
            manaUsedIA = opponentMana;
        }

        //if IA does not move first, combat
        if (!IAFirst)
        {
            //diminish the mana in the board
            ReduceMana(opponentPanel, manaUsedIA, 0);

            Combat();
        }
    }

    //initiative text effect
    public IEnumerator InitiativeText(float seconds)
    {
        //set to make cards not clickable
        isChangingTurn = true;

        initiativeObj.SetActive(true);

        Animator anim = initiativeObj.GetComponent<Animator>();
        anim.SetBool("showText", true);
        yield return new WaitForSeconds(seconds);
        anim.SetBool("showText", false);

        initiativeObj.SetActive(false);

        //can click again
        isChangingTurn = false;
    }

    //change turn effect
    private IEnumerator ChangeTurn(float seconds)
    {
        isChangingTurn = true;
        Animator anim = changeTurn.GetComponent<Animator>();
        anim.SetBool("changeTurn", true);
        yield return new WaitForSeconds(seconds);
        anim.SetBool("changeTurn", false);
        isChangingTurn = false;

        //each player receives 1 mana
        RecoverMana(playerPanel, 1, 20);
        RecoverMana(opponentPanel, 1, 20);
    }

    //back to the menu
    public void GoToMenu()
    {
        SceneManager.LoadScene("menu");
    }

    //combat between the cards!
    private void Combat()
    {
        //if IA played first, need to reduce the mana used
        if (IAFirst)
        {
            ReduceMana(opponentPanel, manaUsedIA, 0);
        }

        //get player card
        GameObject cardPlayer1 = playerBoard.transform.GetChild(turn - 1).transform.GetChild(0).gameObject;
        //get opponent card
        GameObject cardPlayer2 = opponentBoard.transform.GetChild(turn - 1).transform.GetChild(0).gameObject;

        //cards costs
        int cardPlayer1Cost = cardPlayer1.GetComponent<CardController>().cardClass.GetCost();
        int cardPlayer2Cost = cardPlayer2.GetComponent<CardController>().cardClass.GetCost();

        //should disable power for someone?
        bool shouldDisablePlayer = false;
        bool shouldDisableOpponent = false;

        //should blank status for someone?
        bool shouldBlankPlayer = false;
        bool shouldBlankOpponent = false;

        //should copy status for someone?
        bool shouldCopyStatusForPlayer = false;
        bool shouldCopyStatusForOpponent = false;

        //should copy power for someone?
        bool shouldCopyPowerForPlayer = false;
        bool shouldCopyPowerForOpponent = false;

        //change reinforcement
        EffectClass changeReinforcementPlayer = new EffectClass();
        EffectClass changeReinforcementOpponent = new EffectClass();

        //tribal force
        EffectClass tribalForcePlayer = new EffectClass();
        EffectClass tribalForceOpponent = new EffectClass();

        //now, get the attack from the first played card, and defense from the second
        //attack or defense = card attack/defense + (qntManaUsed - card cost)
        //@TODO: disable power deve influenciar aqui tambem!!
        int attack = 0;
        int defense = 0;

        if (IAFirst)
        {
            //test copy power
            shouldCopyPowerForPlayer = CheckCopyPower(cardPlayer1.GetComponent<CardController>(), false);
            shouldCopyPowerForOpponent = CheckCopyPower(cardPlayer2.GetComponent<CardController>(), true);

            //test disable power (if i am copying disable power... whatever...)
            shouldDisablePlayer = CheckDisablePower(cardPlayer2.GetComponent<CardController>(), true);
            shouldDisableOpponent = CheckDisablePower(cardPlayer1.GetComponent<CardController>(), false);

            //test blank status
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                shouldBlankOpponent = CheckBlankStatus(cardPlayer2.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                shouldBlankPlayer = CheckBlankStatus(cardPlayer2.GetComponent<CardController>(), true);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                shouldBlankPlayer = CheckBlankStatus(cardPlayer1.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                shouldBlankOpponent = CheckBlankStatus(cardPlayer1.GetComponent<CardController>(), false);
            }
            //end test blank status

            //test copy status
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                shouldCopyStatusForPlayer = CheckCopyStatus(cardPlayer2.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                shouldCopyStatusForPlayer = CheckCopyStatus(cardPlayer1.GetComponent<CardController>(), false);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                shouldCopyStatusForOpponent = CheckCopyStatus(cardPlayer1.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                shouldCopyStatusForOpponent = CheckCopyStatus(cardPlayer2.GetComponent<CardController>(), true);
            }
            //end test copy status

            //test change reinforcement
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                changeReinforcementPlayer = CheckChangeReinforcement(cardPlayer2.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                changeReinforcementPlayer = CheckChangeReinforcement(cardPlayer1.GetComponent<CardController>(), false);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                changeReinforcementOpponent = CheckChangeReinforcement(cardPlayer1.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                changeReinforcementOpponent = CheckChangeReinforcement(cardPlayer2.GetComponent<CardController>(), true);
            }
            //end test change reinforcement

            //test tribal force
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                tribalForcePlayer = CheckTribalForce(cardPlayer2.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                tribalForcePlayer = CheckTribalForce(cardPlayer1.GetComponent<CardController>(), false);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                tribalForceOpponent = CheckTribalForce(cardPlayer1.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                tribalForceOpponent = CheckTribalForce(cardPlayer2.GetComponent<CardController>(), true);
            }
            //end test tribal force

            //attack and defense
            int cardAttack = cardPlayer2.GetComponent<CardController>().cardClass.GetAttack();
            int cardDefense = cardPlayer1.GetComponent<CardController>().cardClass.GetDefense();

            //if should be copied, invert it
            if (shouldCopyStatusForPlayer)
            {
                cardDefense = cardPlayer2.GetComponent<CardController>().cardClass.GetDefense();
            }
            if (shouldCopyStatusForOpponent)
            {
                cardAttack = cardPlayer1.GetComponent<CardController>().cardClass.GetAttack();
            }

            //if blanked, zero
            if (shouldBlankPlayer)
            {
                cardDefense = 0;
            }
            if (shouldBlankOpponent)
            {
                cardAttack = 0;
            }

            //if the tribal force is status change, add the attack according qnt of race/vocation
            if (tribalForceOpponent.GetGeneralEffect() == "Status Change")
            {
                //find the qnt of race/vocation already in game
                int qntInGame = FindInGame(tribalForceOpponent);

                //if the target is self, normal
                if (tribalForceOpponent.GetTarget()[0] == "Self")
                {
                    attack += tribalForceOpponent.GetAttackModifier() * qntInGame;
                }//otherwise, if it is opponent, other guy
                else if (tribalForceOpponent.GetTarget()[0] == "Opponent")
                {
                    defense += tribalForceOpponent.GetDefenseModifier() * qntInGame;
                }
            }
            if (tribalForcePlayer.GetGeneralEffect() == "Status Change")
            {
                //find the qnt of race/vocation already in game
                int qntInGame = FindInGame(tribalForcePlayer);

                //if the target is self, normal
                if (tribalForcePlayer.GetTarget()[0] == "Self")
                {
                    defense += tribalForcePlayer.GetDefenseModifier() * qntInGame;
                }//otherwise, if it is opponent, other guy
                else if (tribalForcePlayer.GetTarget()[0] == "Opponent")
                {
                    attack += tribalForcePlayer.GetAttackModifier() * qntInGame;
                }
            }

            //if change reinforcement, the attack/defense is calculated without the mana modification
            if (changeReinforcementOpponent.GetGeneralEffect() != "nope") {
                attack += cardAttack;

                //if the change reinforcement is status change, add the attack according mana
                if(changeReinforcementOpponent.GetGeneralEffect() == "Status Change")
                {
                    //if the target is self, normal
                    if (changeReinforcementOpponent.GetTarget()[0] == "Self")
                    {
                        attack += (changeReinforcementOpponent.GetAttackModifier() * (manaUsedIA - cardPlayer2Cost));
                    }//otherwise, if it is opponent, other guy
                    else if (changeReinforcementOpponent.GetTarget()[0] == "Opponent")
                    {
                        defense += (changeReinforcementOpponent.GetDefenseModifier() * (manaUsedIA - cardPlayer2Cost));
                    }
                }
            }//otherwise, normal
            else
            {
                attack += cardAttack + (manaUsedIA - cardPlayer2Cost);
            }

            if (changeReinforcementPlayer.GetGeneralEffect() != "nope")
            {
                defense += cardDefense;

                //if the change reinforcement is status change, add the defense according mana
                if (changeReinforcementPlayer.GetGeneralEffect() == "Status Change")
                {
                    //if the target is self, normal
                    if (changeReinforcementPlayer.GetTarget()[0] == "Self")
                    {
                        defense += (changeReinforcementPlayer.GetDefenseModifier() * (manaUsed - cardPlayer1Cost));
                    }//otherwise, if it is opponent, other guy
                    else if (changeReinforcementPlayer.GetTarget()[0] == "Opponent")
                    {
                        attack += (changeReinforcementPlayer.GetAttackModifier() * (manaUsed - cardPlayer1Cost));
                    }
                }
            }//otherwise, normal
            else
            {
                defense += cardDefense + (manaUsed - cardPlayer1Cost);
            }

            Vector2 statusChange;

            //check for status change, if it is not disabled
            if (!shouldDisableOpponent)
            {
                //if copy power, should test against other player power
                if (shouldCopyPowerForOpponent)
                {
                    statusChange = CheckStatusChange(cardPlayer2.GetComponent<CardController>().cardClass, false, 
                        cardPlayer1.GetComponent<CardController>().cardClass);
                }//otherwise, normal
                else
                {
                    statusChange = CheckStatusChange(cardPlayer1.GetComponent<CardController>().cardClass, true, 
                        cardPlayer2.GetComponent<CardController>().cardClass);
                }

                //if the second value from the method is 1, the effect applies to the opponent
                if (statusChange.y == 0)
                {
                    attack += (int)statusChange.x;
                }
                else if (statusChange.y == 1)
                {
                    defense += (int)statusChange.x;
                }
            }

            //same for the opponent
            if (!shouldDisablePlayer)
            {
                //if copy power, should test against other player power
                if (shouldCopyPowerForPlayer)
                {
                    statusChange = CheckStatusChange(cardPlayer1.GetComponent<CardController>().cardClass, true,
                        cardPlayer2.GetComponent<CardController>().cardClass);
                }//otherwise, normal
                else
                {
                    statusChange = CheckStatusChange(cardPlayer2.GetComponent<CardController>().cardClass, false,
                        cardPlayer1.GetComponent<CardController>().cardClass);
                }
                
                if (statusChange.y == 0)
                {
                    defense += (int)statusChange.x;
                }
                else if (statusChange.y == 1)
                {
                    attack += (int)statusChange.x;
                }
            }

            //check the status change for the hero too
            //player
            statusChange = CheckStatusChange(cardPlayer2.GetComponent<CardController>().cardClass, false, null, playerHero);
            if (statusChange.y == 0)
            {
                defense += (int)statusChange.x;
            }
            else if (statusChange.y == 1)
            {
                attack += (int)statusChange.x;
            }
            //opponent
            statusChange = CheckStatusChange(cardPlayer1.GetComponent<CardController>().cardClass, true, null, opponentHero);
            if (statusChange.y == 0)
            {
                attack += (int)statusChange.x;
            }
            else if (statusChange.y == 1)
            {
                defense += (int)statusChange.x;
            }
        }//otherwise...
        else
        {
            //test copy power
            shouldCopyPowerForPlayer = CheckCopyPower(cardPlayer1.GetComponent<CardController>(), false);
            shouldCopyPowerForOpponent = CheckCopyPower(cardPlayer2.GetComponent<CardController>(), true);

            //test disable power (if i am copying disable power... whatever...)
            shouldDisablePlayer = CheckDisablePower(cardPlayer2.GetComponent<CardController>(), false);
            shouldDisableOpponent = CheckDisablePower(cardPlayer1.GetComponent<CardController>(), true);

            //test blank status
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                shouldBlankOpponent = CheckBlankStatus(cardPlayer2.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                shouldBlankPlayer = CheckBlankStatus(cardPlayer2.GetComponent<CardController>(), false);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                shouldBlankPlayer = CheckBlankStatus(cardPlayer1.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                shouldBlankOpponent = CheckBlankStatus(cardPlayer1.GetComponent<CardController>(), true);
            }
            //end test blank status

            //test copy status
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                shouldCopyStatusForPlayer = CheckCopyStatus(cardPlayer2.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                shouldCopyStatusForPlayer = CheckCopyStatus(cardPlayer1.GetComponent<CardController>(), true);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                shouldCopyStatusForOpponent = CheckCopyStatus(cardPlayer1.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                shouldCopyStatusForOpponent = CheckCopyStatus(cardPlayer2.GetComponent<CardController>(), false);
            }
            //end test copy status

            //test change reinforcement
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                changeReinforcementPlayer = CheckChangeReinforcement(cardPlayer2.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                changeReinforcementPlayer = CheckChangeReinforcement(cardPlayer1.GetComponent<CardController>(), true);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                changeReinforcementOpponent = CheckChangeReinforcement(cardPlayer1.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                changeReinforcementOpponent = CheckChangeReinforcement(cardPlayer2.GetComponent<CardController>(), false);
            }
            //end test change reinforcement

            //test tribal force
            //if player copy power, should test against opponent power
            if (shouldCopyPowerForPlayer)
            {
                tribalForcePlayer = CheckTribalForce(cardPlayer2.GetComponent<CardController>(), false);
            }//otherwise, normal
            else
            {
                tribalForcePlayer = CheckTribalForce(cardPlayer1.GetComponent<CardController>(), true);
            }

            //same for opponent
            if (shouldCopyPowerForOpponent)
            {
                tribalForceOpponent = CheckTribalForce(cardPlayer1.GetComponent<CardController>(), true);
            }//otherwise, normal
            else
            {
                tribalForceOpponent = CheckTribalForce(cardPlayer2.GetComponent<CardController>(), false);
            }
            //end test tribal force

            //attack and defense
            int cardAttack = cardPlayer1.GetComponent<CardController>().cardClass.GetAttack();
            int cardDefense = cardPlayer2.GetComponent<CardController>().cardClass.GetDefense();
            
            //if should be copied, invert it
            if (shouldCopyStatusForPlayer)
            {
                cardAttack = cardPlayer2.GetComponent<CardController>().cardClass.GetAttack();
            }
            if (shouldCopyStatusForOpponent)
            {
                cardDefense = cardPlayer1.GetComponent<CardController>().cardClass.GetDefense();
            }

            //if blanked, zero
            if (shouldBlankPlayer)
            {
                cardAttack = 0;
            }
            if (shouldBlankOpponent)
            {
                cardDefense = 0;
            }

            //if the tribal force is status change, add the attack according qnt of race/vocation
            if (tribalForceOpponent.GetGeneralEffect() == "Status Change")
            {
                //find the qnt of race/vocation already in game
                int qntInGame = FindInGame(tribalForceOpponent);

                //if the target is self, normal
                if (tribalForceOpponent.GetTarget()[0] == "Self")
                {
                    defense += tribalForceOpponent.GetDefenseModifier() * qntInGame;
                }//otherwise, if it is opponent, other guy
                else if (tribalForceOpponent.GetTarget()[0] == "Opponent")
                {
                    attack += tribalForceOpponent.GetAttackModifier() * qntInGame;
                }
            }
            if (tribalForcePlayer.GetGeneralEffect() == "Status Change")
            {
                //find the qnt of race/vocation already in game
                int qntInGame = FindInGame(tribalForcePlayer);

                //if the target is self, normal
                if (tribalForcePlayer.GetTarget()[0] == "Self")
                {
                    attack += tribalForcePlayer.GetAttackModifier() * qntInGame;
                }//otherwise, if it is opponent, other guy
                else if (tribalForcePlayer.GetTarget()[0] == "Opponent")
                {
                    defense += tribalForcePlayer.GetDefenseModifier() * qntInGame;
                }
            }

            //if change reinforcement, the attack/defense is calculated without the mana modification
            if (changeReinforcementOpponent.GetGeneralEffect() != "nope")
            {
                defense += cardDefense;

                //if the change reinforcement is status change, add the attack according mana
                if (changeReinforcementOpponent.GetGeneralEffect() == "Status Change")
                {
                    //if the target is self, normal
                    if (changeReinforcementOpponent.GetTarget()[0] == "Self")
                    {
                        defense += (changeReinforcementOpponent.GetDefenseModifier() * (manaUsedIA - cardPlayer2Cost));
                    }//otherwise, if it is opponent, other guy
                    else if (changeReinforcementOpponent.GetTarget()[0] == "Opponent")
                    {
                        attack += (changeReinforcementOpponent.GetAttackModifier() * (manaUsedIA - cardPlayer2Cost));
                    }
                }
            }//otherwise, normal
            else
            {
                defense += cardDefense + (manaUsedIA - cardPlayer2Cost);
            }

            if (changeReinforcementPlayer.GetGeneralEffect() != "nope")
            {
                attack += cardAttack;

                //if the change reinforcement is status change, add the defense according mana
                if (changeReinforcementPlayer.GetGeneralEffect() == "Status Change")
                {
                    //if the target is self, normal
                    if (changeReinforcementPlayer.GetTarget()[0] == "Self")
                    {
                        attack += (changeReinforcementPlayer.GetAttackModifier() * (manaUsed - cardPlayer1Cost));
                    }//otherwise, if it is opponent, other guy
                    else if (changeReinforcementPlayer.GetTarget()[0] == "Opponent")
                    {
                        defense += (changeReinforcementPlayer.GetDefenseModifier() * (manaUsed - cardPlayer1Cost));
                    }
                }
            }//otherwise, normal
            else
            {
                attack += cardAttack + (manaUsed - cardPlayer1Cost);
            }
            
            Vector2 statusChange;

            //check for status change if not disabled
            if (!shouldDisablePlayer)
            {
                //if copy power, should test against other player power
                if (shouldCopyPowerForPlayer)
                {
                    statusChange = CheckStatusChange(cardPlayer1.GetComponent<CardController>().cardClass, false,
                        cardPlayer2.GetComponent<CardController>().cardClass);
                }//otherwise, normal
                else
                {
                    statusChange = CheckStatusChange(cardPlayer2.GetComponent<CardController>().cardClass, true,
                        cardPlayer1.GetComponent<CardController>().cardClass);
                }

                //if the second value from the method is 1, the effect applies to the opponent
                if (statusChange.y == 0)
                {
                    attack += (int)statusChange.x;
                }
                else if (statusChange.y == 1)
                {
                    defense += (int)statusChange.x;
                }
            }

            //same for the opponent
            if (!shouldDisableOpponent)
            {
                //if copy power, should test against other player power
                if (shouldCopyPowerForOpponent)
                {
                    statusChange = CheckStatusChange(cardPlayer2.GetComponent<CardController>().cardClass, true,
                        cardPlayer1.GetComponent<CardController>().cardClass);
                }//otherwise, normal
                else
                {
                    statusChange = CheckStatusChange(cardPlayer1.GetComponent<CardController>().cardClass, false,
                        cardPlayer2.GetComponent<CardController>().cardClass);
                }
                
                if (statusChange.y == 0)
                {
                    defense += (int)statusChange.x;
                }
                else if (statusChange.y == 1)
                {
                    attack += (int)statusChange.x;
                }
            }

            //check the status change for the hero too
            //player
            statusChange = CheckStatusChange(cardPlayer2.GetComponent<CardController>().cardClass, true, null, playerHero);
            if (statusChange.y == 0)
            {
                attack += (int)statusChange.x;
            }
            else if (statusChange.y == 1)
            {
                defense += (int)statusChange.x;
            }
            //opponent
            statusChange = CheckStatusChange(cardPlayer1.GetComponent<CardController>().cardClass, false, null, opponentHero);
            if (statusChange.y == 0)
            {
                defense += (int)statusChange.x;
            }
            else if (statusChange.y == 1)
            {
                attack += (int)statusChange.x;
            }
        }

        //set player and opponent values, to show later in versus panel
        int playerValue, opponentValue = 0;
        if (IAFirst)
        {
            playerValue = defense;
            opponentValue = attack;
        }
        else
        {
            playerValue = attack;
            opponentValue = defense;
        }

        //damage
        int damage = attack - defense;

        //if damage is positive, discount it from the defender
        if(damage > 0)
        {
            if (IAFirst)
            {
                //check the mana and life conditions for the player, if not disabled
                if (!shouldDisablePlayer)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForPlayer)
                    {
                        ManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero, true);
                        LifeEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero, true);
                        StealManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero);
                        LifeEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero);
                        StealManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero);
                    }
                }

                //the same for the opponent
                if (!shouldDisableOpponent)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForOpponent)
                    {
                        ManaEffect("Opponent", true, true, cardPlayer1, cardPlayer2, opponentHero, true);
                        LifeEffect("Opponent", true, true, cardPlayer1, cardPlayer2, opponentHero, true);
                        StealManaEffect("Opponent", true, true, cardPlayer1, cardPlayer2, opponentHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Opponent", true, true, cardPlayer1, cardPlayer2, opponentHero);
                        LifeEffect("Opponent", true, true, cardPlayer1, cardPlayer2, opponentHero);
                        StealManaEffect("Opponent", true, true, cardPlayer1, cardPlayer2, opponentHero);
                    }
                }

                //diminish the life in the board
                ReduceLife(playerPanel, damage, 0);
            }
            else
            {
                //check the mana and life conditions for the player, if not disabled
                if (!shouldDisablePlayer)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForPlayer)
                    {
                        ManaEffect("Player", true, true, cardPlayer1, cardPlayer2, playerHero, true);
                        LifeEffect("Player", true, true, cardPlayer1, cardPlayer2, playerHero, true);
                        StealManaEffect("Player", true, true, cardPlayer1, cardPlayer2, playerHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Player", true, true, cardPlayer1, cardPlayer2, playerHero);
                        LifeEffect("Player", true, true, cardPlayer1, cardPlayer2, playerHero);
                        StealManaEffect("Player", true, true, cardPlayer1, cardPlayer2, playerHero);
                    }
                }

                //the same for the opponent
                if (!shouldDisableOpponent)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForOpponent)
                    {
                        ManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        LifeEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        StealManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero);
                        LifeEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero);
                        StealManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero);
                    }
                }

                //diminish the life in the board
                ReduceLife(opponentPanel, damage, 0);
            }
        }//else, if the damage is negative, the attacker loses health
        else if(damage < 0)
        {
            if (IAFirst)
            {
                //check the mana and life conditions for the player, if not disabled
                if (!shouldDisablePlayer)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForPlayer)
                    {
                        ManaEffect("Player", false, true, cardPlayer1, cardPlayer2, playerHero, true);
                        LifeEffect("Player", false, true, cardPlayer1, cardPlayer2, playerHero, true);
                        StealManaEffect("Player", false, true, cardPlayer1, cardPlayer2, playerHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Player", false, true, cardPlayer1, cardPlayer2, playerHero);
                        LifeEffect("Player", false, true, cardPlayer1, cardPlayer2, playerHero);
                        StealManaEffect("Player", false, true, cardPlayer1, cardPlayer2, playerHero);
                    }
                }

                //the same for the opponent
                if (!shouldDisableOpponent)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForOpponent)
                    {
                        ManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        LifeEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        StealManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero);
                        LifeEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero);
                        StealManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero);
                    }
                }

                //diminish the life in the board
                ReduceLife(opponentPanel, damage, 0);
            }
            else
            {
                //check the mana and life conditions for the player, if not disabled
                if (!shouldDisablePlayer)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForPlayer)
                    {
                        ManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero, true);
                        LifeEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero, true);
                        StealManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero);
                        LifeEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero);
                        StealManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero);
                    }
                }

                //the same for the opponent
                if (!shouldDisableOpponent)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForOpponent)
                    {
                        ManaEffect("Opponent", false, true, cardPlayer1, cardPlayer2, opponentHero, true);
                        LifeEffect("Opponent", false, true, cardPlayer1, cardPlayer2, opponentHero, true);
                        StealManaEffect("Opponent", false, true, cardPlayer1, cardPlayer2, opponentHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Opponent", false, true, cardPlayer1, cardPlayer2, opponentHero);
                        LifeEffect("Opponent", false, true, cardPlayer1, cardPlayer2, opponentHero);
                        StealManaEffect("Opponent", false, true, cardPlayer1, cardPlayer2, opponentHero);
                    }
                }

                //diminish the life in the board
                ReduceLife(playerPanel, damage, 0);
            }
        }//else, damage is 0, but the mana and life effect can still apply
        else
        {
            if (IAFirst)
            {
                //check the mana and life conditions for the player, if not disabled
                if (!shouldDisablePlayer)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForPlayer)
                    {
                        ManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero, true);
                        LifeEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero, true);
                        StealManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero);
                        LifeEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero);
                        StealManaEffect("Player", false, false, cardPlayer1, cardPlayer2, playerHero);
                    }
                }

                //the same for the opponent
                if (!shouldDisableOpponent)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForOpponent)
                    {
                        ManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        LifeEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        StealManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero);
                        LifeEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero);
                        StealManaEffect("Opponent", true, false, cardPlayer1, cardPlayer2, opponentHero);
                    }
                }
            }
            else
            {
                //check the mana and life conditions for the player, if not disabled
                if (!shouldDisablePlayer)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForPlayer)
                    {
                        ManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero, true);
                        LifeEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero, true);
                        StealManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero);
                        LifeEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero);
                        StealManaEffect("Player", true, false, cardPlayer1, cardPlayer2, playerHero);
                    }
                }

                //the same for the opponent
                if (!shouldDisableOpponent)
                {
                    //if copy power, should invert
                    if (shouldCopyPowerForOpponent)
                    {
                        ManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        LifeEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero, true);
                        StealManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero, true);
                    }//otherwise, normal
                    else
                    {
                        ManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero);
                        LifeEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero);
                        StealManaEffect("Opponent", false, false, cardPlayer1, cardPlayer2, opponentHero);
                    }
                }
            }
        }

        //if change reinforcement for player is life change, apply
        if (changeReinforcementPlayer.GetGeneralEffect() == "Life Change" && !shouldDisablePlayer)
        {
            //get the value
            int lifeValue = changeReinforcementPlayer.GetValueModifier() * (manaUsed - cardPlayer1Cost);

            //if target is self, normal
            if (changeReinforcementPlayer.GetTarget()[0] == "Self")
            {
                RecoverLife(playerPanel, lifeValue, changeReinforcementPlayer.GetMaxModifier());
            }//otherwise, other guy
            else if (changeReinforcementPlayer.GetTarget()[0] == "Opponent")
            {
                ReduceLife(opponentPanel, lifeValue, changeReinforcementPlayer.GetMinModifier());
            }
        }
        //if change reinforcement for player is mana change, apply
        if (changeReinforcementPlayer.GetGeneralEffect() == "Mana Change" && !shouldDisablePlayer)
        {
            //get the value
            int manaValue = changeReinforcementPlayer.GetValueModifier() * (manaUsed - cardPlayer1Cost);

            //if target is self, normal
            if (changeReinforcementPlayer.GetTarget()[0] == "Self")
            {
                RecoverMana(playerPanel, manaValue, changeReinforcementPlayer.GetMaxModifier());
            }//otherwise, other guy
            else if (changeReinforcementPlayer.GetTarget()[0] == "Opponent")
            {
                ReduceMana(opponentPanel, manaValue, changeReinforcementPlayer.GetMinModifier());
            }
        }

        //same for opponent
        if (changeReinforcementOpponent.GetGeneralEffect() == "Life Change" && !shouldDisableOpponent)
        {
            //get the value
            int lifeValue = changeReinforcementOpponent.GetValueModifier() * (manaUsedIA - cardPlayer2Cost);

            //if target is self, normal
            if (changeReinforcementOpponent.GetTarget()[0] == "Self")
            {
                RecoverLife(opponentPanel, lifeValue, changeReinforcementPlayer.GetMaxModifier());
            }//otherwise, other guy
            else if (changeReinforcementOpponent.GetTarget()[0] == "Opponent")
            {
                ReduceLife(playerPanel, lifeValue, changeReinforcementOpponent.GetMinModifier());
            }
        }
        if (changeReinforcementOpponent.GetGeneralEffect() == "Mana Change" && !shouldDisableOpponent)
        {
            //get the value
            int manaValue = changeReinforcementOpponent.GetValueModifier() * (manaUsedIA - cardPlayer2Cost);

            //if target is self, normal
            if (changeReinforcementOpponent.GetTarget()[0] == "Self")
            {
                RecoverMana(opponentPanel, manaValue, changeReinforcementPlayer.GetMaxModifier());
            }//otherwise, other guy
            else if (changeReinforcementOpponent.GetTarget()[0] == "Opponent")
            {
                ReduceMana(playerPanel, manaValue, changeReinforcementOpponent.GetMinModifier());
            }
        }

        //if tribal force for player is life change, apply
        if (tribalForcePlayer.GetGeneralEffect() == "Life Change" && !shouldDisablePlayer)
        {
            //find the qnt of race/vocation already in game
            int qntInGame = FindInGame(tribalForcePlayer);

            //get the value
            int lifeValue = tribalForcePlayer.GetValueModifier() * qntInGame;

            //if target is self, normal
            if (tribalForcePlayer.GetTarget()[0] == "Self")
            {
                RecoverLife(playerPanel, lifeValue, tribalForcePlayer.GetMaxModifier());
            }//otherwise, other guy
            else if (tribalForcePlayer.GetTarget()[0] == "Opponent")
            {
                ReduceLife(opponentPanel, lifeValue, tribalForcePlayer.GetMinModifier());
            }
        }
        //if change reinforcement for player is mana change, apply
        if (tribalForcePlayer.GetGeneralEffect() == "Mana Change" && !shouldDisablePlayer)
        {
            //find the qnt of race/vocation already in game
            int qntInGame = FindInGame(tribalForcePlayer);

            //get the value
            int manaValue = tribalForcePlayer.GetValueModifier() * qntInGame;

            //if target is self, normal
            if (tribalForcePlayer.GetTarget()[0] == "Self")
            {
                RecoverMana(playerPanel, manaValue, tribalForcePlayer.GetMaxModifier());
            }//otherwise, other guy
            else if (tribalForcePlayer.GetTarget()[0] == "Opponent")
            {
                ReduceMana(opponentPanel, manaValue, tribalForcePlayer.GetMinModifier());
            }
        }

        //same for opponent
        if (tribalForceOpponent.GetGeneralEffect() == "Life Change" && !shouldDisableOpponent)
        {
            //find the qnt of race/vocation already in game
            int qntInGame = FindInGame(tribalForceOpponent);

            //get the value
            int lifeValue = tribalForceOpponent.GetValueModifier() * qntInGame;

            //if target is self, normal
            if (tribalForceOpponent.GetTarget()[0] == "Self")
            {
                RecoverLife(opponentPanel, lifeValue, tribalForceOpponent.GetMaxModifier());
            }//otherwise, other guy
            else if (tribalForceOpponent.GetTarget()[0] == "Opponent")
            {
                ReduceLife(playerPanel, lifeValue, tribalForceOpponent.GetMinModifier());
            }
        }
        if (tribalForceOpponent.GetGeneralEffect() == "Mana Change" && !shouldDisableOpponent)
        {
            //find the qnt of race/vocation already in game
            int qntInGame = FindInGame(tribalForceOpponent);

            //get the value
            int manaValue = tribalForceOpponent.GetValueModifier() * qntInGame;

            //if target is self, normal
            if (tribalForceOpponent.GetTarget()[0] == "Self")
            {
                RecoverMana(opponentPanel, manaValue, tribalForceOpponent.GetMaxModifier());
            }//otherwise, other guy
            else if (tribalForceOpponent.GetTarget()[0] == "Opponent")
            {
                ReduceMana(playerPanel, manaValue, tribalForceOpponent.GetMinModifier());
            }
        }

        //combat panel
        StartCoroutine(CombatPanel(cardPlayer1, cardPlayer2, 5, playerValue, opponentValue));
    }

    //combat panel
    private IEnumerator CombatPanel(GameObject cardPlayer1, GameObject cardPlayer2, int seconds, int playerValue, int opponentValue)
    {
        //show the combat panel
        combatPanel.SetActive(true);

        //set the info for player
        playerBattlePanel.transform.Find("PlayerBattleHero").gameObject.GetComponent<Image>().sprite =
            Resources.Load<Sprite>(playerHero.GetName());
        playerBattlePanel.transform.Find("PlayerBattleHeroEffect").gameObject.GetComponent<Text>().text = playerHero.GetName() + '\n';
        foreach (EffectClass he in playerHero.GetEffect())
        {
            playerBattlePanel.transform.Find("PlayerBattleHeroEffect").gameObject.GetComponent<Text>().text += he.GetEffect() + '\n';
        }
        playerBattlePanel.transform.Find("PlayerBattleCard").gameObject.GetComponent<Image>().material =
            Resources.Load<Material>("Cards/Materials/" + cardPlayer1.GetComponent<CardController>().cardClass.GetName());
        playerBattlePanel.transform.Find("PlayerBattleCardEffect").gameObject.GetComponent<Text>().text =
            cardPlayer1.GetComponent<CardController>().cardClass.GetName() + " - " + 
            cardPlayer1.GetComponent<CardController>().cardClass.GetCost() + '\n';
        if (cardPlayer1.GetComponent<CardController>().cardClass.GetEffects() != null)
        {
            foreach (EffectClass ce in cardPlayer1.GetComponent<CardController>().cardClass.GetEffects())
            {
                playerBattlePanel.transform.Find("PlayerBattleCardEffect").gameObject.GetComponent<Text>().text += ce.GetEffect() + '\n';
            }
        }
        playerBattlePanel.transform.Find("PlayerBattleCardEffect").gameObject.GetComponent<Text>().text +=
            cardPlayer1.GetComponent<CardController>().cardClass.GetAttack() + "/" + 
            cardPlayer1.GetComponent<CardController>().cardClass.GetDefense();

        //set the info for opponent
        opponentBattlePanel.transform.Find("OpponentBattleHero").gameObject.GetComponent<Image>().sprite =
            Resources.Load<Sprite>(opponentHero.GetName());
        opponentBattlePanel.transform.Find("OpponentBattleHeroEffect").gameObject.GetComponent<Text>().text = opponentHero.GetName() + '\n';
        foreach (EffectClass he in opponentHero.GetEffect())
        {
            opponentBattlePanel.transform.Find("OpponentBattleHeroEffect").gameObject.GetComponent<Text>().text += he.GetEffect() + '\n';
        }
        opponentBattlePanel.transform.Find("OpponentBattleCard").gameObject.GetComponent<Image>().material =
            Resources.Load<Material>("Cards/Materials/" + cardPlayer2.GetComponent<CardController>().cardClass.GetName());
        opponentBattlePanel.transform.Find("OpponentBattleCardEffect").gameObject.GetComponent<Text>().text =
            cardPlayer2.GetComponent<CardController>().cardClass.GetName() + " - " +
            cardPlayer2.GetComponent<CardController>().cardClass.GetCost() + '\n';
        if (cardPlayer2.GetComponent<CardController>().cardClass.GetEffects() != null)
        {
            foreach (EffectClass ce in cardPlayer2.GetComponent<CardController>().cardClass.GetEffects())
            {
                opponentBattlePanel.transform.Find("OpponentBattleCardEffect").gameObject.GetComponent<Text>().text += ce.GetEffect() + '\n';
            }
        }
        opponentBattlePanel.transform.Find("OpponentBattleCardEffect").gameObject.GetComponent<Text>().text +=
            cardPlayer2.GetComponent<CardController>().cardClass.GetAttack() + "/" +
            cardPlayer2.GetComponent<CardController>().cardClass.GetDefense();

        //set the versus values
        combatPanel.transform.Find("PlayerValue").GetComponent<Text>().text = playerValue.ToString();
        combatPanel.transform.Find("OpponentValue").GetComponent<Text>().text = opponentValue.ToString();
        
        //show damage
        StartCoroutine(ShowDamage(2, playerValue, opponentValue, combatPanel.transform.Find("PlayerValue").gameObject,
            combatPanel.transform.Find("OpponentValue").gameObject));

        //wait
        yield return new WaitForSeconds(seconds);

        //reset combat values
        ResetValuesCombat(combatPanel.transform.Find("PlayerValue").gameObject, combatPanel.transform.Find("OpponentValue").gameObject);

        //hide panel
        combatPanel.SetActive(false);

        //if any player has 0 life, game is over
        if (playerHealth <= 0)
        {
            gameOver = true;

            //set the text
            gameOverObj.GetComponent<Text>().text = "Computer wins!!!";
        }
        else if (opponentHealth <= 0)
        {
            gameOver = true;

            //set the text
            gameOverObj.GetComponent<Text>().text = "Player wins!!!";
        }//else, if all cards were played, game is also over. Player with more life wins
        else if (turn == 4)
        {
            gameOver = true;

            if (playerHealth > opponentHealth)
            {
                //set the text
                gameOverObj.GetComponent<Text>().text = "Player wins!!!";
            }
            else if (opponentHealth > playerHealth)
            {
                //set the text
                gameOverObj.GetComponent<Text>().text = "Computer wins!!!";
            }//else, draw
            else
            {
                //set the text
                gameOverObj.GetComponent<Text>().text = "It's a draw!!!";
            }
        }//else, prepare for next turn
        else
        {
            //update turn and change play order
            turn++;
            IAFirst = !IAFirst;

            //reset mana Used
            manaUsed = manaUsedIA = 0;

            //change turn text
            changeTurn.GetComponent<Text>().text = "Turn " + turn;

            //change turn effect
            //wait 1 seconds
            StartCoroutine(ChangeTurn(1));

            //now that order changed, if IA plays first, play
            if (IAFirst)
            {
                IAMove();
            }
        }

        //if game over, show the screen
        if (gameOver)
        {
            gameOverObj.SetActive(true);
            menuButton.SetActive(true);

            //animation
            gameOverObj.GetComponent<Animator>().SetBool("gameOver", true);
        }
    }

    //show the damage in combat
    private IEnumerator ShowDamage(int seconds, int playerValue, int opponentValue, GameObject playerValueText, GameObject opponentValueText)
    {
        //versus fade
        combatPanel.transform.Find("Versus").gameObject.GetComponent<Animator>().SetBool("fading", true);

        //wait 1 second
        yield return new WaitForSeconds(1);

        //if player value is higher, opponent takes damage
        if (playerValue > opponentValue)
        {
            opponentValueText.GetComponent<Animator>().SetBool("damageMove", true);

            //the other fades
            playerValueText.GetComponent<Animator>().SetBool("fading", true);

            //damage value
            opponentValueText.GetComponent<Text>().text = (playerValue - opponentValue).ToString();
        }//else, player takes damage
        else if (playerValue < opponentValue)
        {
            playerValueText.GetComponent<Animator>().SetBool("damageMove", true);

            //the other fades
            opponentValueText.GetComponent<Animator>().SetBool("fading", true);

            //damage value
            playerValueText.GetComponent<Text>().text = (opponentValue - playerValue).ToString();
        }//else, its a draw
        else
        {
            playerValueText.GetComponent<Animator>().SetBool("damageMove", true);

            //the other fades
            opponentValueText.GetComponent<Animator>().SetBool("fading", true);

            //damage value
            playerValueText.GetComponent<Text>().text = "0";
        }

        yield return new WaitForSeconds(seconds);
    }

    //reset player and opponent values in combat
    private void ResetValuesCombat(GameObject playerValueText, GameObject opponentValueText)
    {
        //reset scale and position of player combat value
        playerValueText.GetComponent<RectTransform>().anchoredPosition = new Vector2(-150, 0);
        playerValueText.GetComponent<RectTransform>().localScale = Vector3.one;

        //reset scale and position of opponent combat value
        opponentValueText.GetComponent<RectTransform>().anchoredPosition = new Vector2(120, 0);
        opponentValueText.GetComponent<RectTransform>().localScale = Vector3.one;

        //reset versus scale
        combatPanel.transform.Find("Versus").GetComponent<RectTransform>().localScale = Vector3.one;
    }

    //find the qnt of race/vocation in game for tribal force
    private int FindInGame(EffectClass tribalForce)
    {
        int qntInGame = 0;

        //for each card already played for player
        foreach(Transform tp in playerBoard.transform)
        {
            //transform has Place, child has the card
            if (tp.childCount > 0)
            {
                GameObject cp = tp.GetChild(0).gameObject;

                //check if it is from some race for this tribal force
                if (tribalForce.GetIngameRace().Contains(cp.GetComponent<CardController>().cardClass.GetRace()))
                {
                    //increment
                    qntInGame++;
                }//same for vocation
                else if (tribalForce.GetIngameVocation().Contains(cp.GetComponent<CardController>().cardClass.GetVocation()))
                {
                    //increment
                    qntInGame++;
                }
            }
        }

        //same for opponent cards played
        foreach (Transform tp in opponentBoard.transform)
        {
            //transform has Place, child has the card
            if (tp.childCount > 0)
            {
                GameObject cp = tp.GetChild(0).gameObject;

                //check if it is from some race for this tribal force
                if (tribalForce.GetIngameRace().Contains(cp.GetComponent<CardController>().cardClass.GetRace()))
                {
                    //increment
                    qntInGame++;
                }//same for vocation
                else if (tribalForce.GetIngameVocation().Contains(cp.GetComponent<CardController>().cardClass.GetVocation()))
                {
                    //increment
                    qntInGame++;
                }
            }
        }

        //check both heroes
        //check if it is from some race for this tribal force
        if (tribalForce.GetIngameRace().Contains(playerHero.GetRace()))
        {
            //increment
            qntInGame++;
        }//same for vocation
        else if (tribalForce.GetIngameVocation().Contains(playerHero.GetVocation()))
        {
            //increment
            qntInGame++;
        }
        if (tribalForce.GetIngameRace().Contains(opponentHero.GetRace()))
        {
            //increment
            qntInGame++;
        }//same for vocation
        else if (tribalForce.GetIngameVocation().Contains(opponentHero.GetVocation()))
        {
            //increment
            qntInGame++;
        }

        return qntInGame;
    }

    //life effect due to the life change
    //target: which card is being tested (player or opponent)
    //bool parameters: indicates if target attacks and/or wins. Invert: when Copy Power is used
    private void LifeEffect(string target, bool targetAttacks, bool targetWins, GameObject cardPlayer1, GameObject cardPlayer2,
        HeroClass heroToCheck = null, bool invert = false)
    {
        //chooses targets
        GameObject cardTarget = cardPlayer1;
        GameObject otherGuy = cardPlayer2;
        GameObject panelTarget = playerPanel;
        GameObject panelOther = opponentPanel;
        if (target == "Opponent")
        {
            cardTarget = cardPlayer2;
            otherGuy = cardPlayer1;
            panelTarget = opponentPanel;
            panelOther = playerPanel;
        }

        //check the mana conditions for the player
        List<Vector2> checkLC = new List<Vector2>();

        //if invert, should test against the other player card
        if (invert)
        {
            checkLC = CheckLifeChange(targetAttacks, targetWins, otherGuy.GetComponent<CardController>().cardClass);
        }//otherwise, normal
        else
        {
            checkLC = CheckLifeChange(targetAttacks, targetWins, cardTarget.GetComponent<CardController>().cardClass);
        }
        
        Vector2 lifeChange = checkLC[0];
        //if the second value from the method is 1, the effect applies to the opponent
        if (lifeChange.y == 0)
        {
            //update mana
            if (lifeChange.x > 0)
            {
                RecoverLife(panelTarget, (int)lifeChange.x, (int)checkLC[1].y);
            }
            else if (lifeChange.x < 0)
            {
                ReduceLife(panelTarget, (int)lifeChange.x, (int)checkLC[1].x);
            }
        }
        else if (lifeChange.y == 1)
        {
            //update mana
            if (lifeChange.x > 0)
            {
                RecoverLife(panelOther, (int)lifeChange.x, (int)checkLC[1].y);
            }
            else if (lifeChange.x < 0)
            {
                ReduceLife(panelOther, (int)lifeChange.x, (int)checkLC[1].x);
            }
        }

        //same for the hero
        if(heroToCheck != null)
        {
            checkLC = CheckLifeChange(targetAttacks, targetWins, null, heroToCheck);
            lifeChange = checkLC[0];

            if (lifeChange.y == 0)
            {
                //update mana
                if (lifeChange.x > 0)
                {
                    RecoverLife(panelTarget, (int)lifeChange.x, (int)checkLC[1].y);
                }
                else if (lifeChange.x < 0)
                {
                    ReduceLife(panelTarget, (int)lifeChange.x, (int)checkLC[1].x);
                }
            }
            else if (lifeChange.y == 1)
            {
                //update mana
                if (lifeChange.x > 0)
                {
                    RecoverLife(panelOther, (int)lifeChange.x, (int)checkLC[1].y);
                }
                else if (lifeChange.x < 0)
                {
                    ReduceLife(panelOther, (int)lifeChange.x, (int)checkLC[1].x);
                }
            }
        }
    }

    //mana effect due to the mana change
    //target: which card is being tested (player of opponent)
    //bool parameters: indicates if target attacks and/or wins. Invert: when Copy Power is used
    private void ManaEffect(string target, bool targetAttacks, bool targetWins, GameObject cardPlayer1, GameObject cardPlayer2,
        HeroClass heroToCheck, bool invert = false)
    {
        //chooses targets
        GameObject cardTarget = cardPlayer1;
        GameObject otherGuy = cardPlayer2;
        GameObject panelTarget = playerPanel;
        GameObject panelOther = opponentPanel;
        if (target == "Opponent")
        {
            cardTarget = cardPlayer2;
            otherGuy = cardPlayer1;
            panelTarget = opponentPanel;
            panelOther = playerPanel;
        }

        //check the mana conditions for the player
        List<Vector2> checkMC = new List<Vector2>();

        //if invert, should test against the other player card
        if (invert)
        {
            checkMC = CheckManaChange(targetAttacks, targetWins, otherGuy.GetComponent<CardController>().cardClass);
        }//otherwise, normal
        else
        {
            checkMC = CheckManaChange(targetAttacks, targetWins, cardTarget.GetComponent<CardController>().cardClass);
        }

        Vector2 manaChange = checkMC[0];
        //if the second value from the method is 1, the effect applies to the opponent
        if (manaChange.y == 0)
        {
            //update mana
            if (manaChange.x > 0)
            {
                RecoverMana(panelTarget, (int)manaChange.x, (int)checkMC[1].y);
            }
            else if (manaChange.x < 0)
            {
                ReduceMana(panelTarget, (int)manaChange.x, (int)checkMC[1].x);
            }
        }
        else if (manaChange.y == 1)
        {
            //update mana
            if (manaChange.x > 0)
            {
                RecoverMana(panelOther, (int)manaChange.x, (int)checkMC[1].y);
            }
            else if (manaChange.x < 0)
            {
                ReduceMana(panelOther, (int)manaChange.x, (int)checkMC[1].x);
            }
        }

        //check for the hero too
        if (heroToCheck != null)
        {
            checkMC = CheckManaChange(targetAttacks, targetWins, null, heroToCheck);
            manaChange = checkMC[0];
            //if the second value from the method is 1, the effect applies to the opponent
            if (manaChange.y == 0)
            {
                //update mana
                if (manaChange.x > 0)
                {
                    RecoverMana(panelTarget, (int)manaChange.x, (int)checkMC[1].y);
                }
                else if (manaChange.x < 0)
                {
                    ReduceMana(panelTarget, (int)manaChange.x, (int)checkMC[1].x);
                }
            }
            else if (manaChange.y == 1)
            {
                //update mana
                if (manaChange.x > 0)
                {
                    RecoverMana(panelOther, (int)manaChange.x, (int)checkMC[1].y);
                }
                else if (manaChange.x < 0)
                {
                    ReduceMana(panelOther, (int)manaChange.x, (int)checkMC[1].x);
                }
            }
        }
    }

    //steal mana effect due to the steal mana
    //target: which card is being tested (player of opponent)
    //bool parameters: indicates if target attacks and/or wins. Invert: when Copy Power is used
    private void StealManaEffect(string target, bool targetAttacks, bool targetWins, GameObject cardPlayer1, GameObject cardPlayer2,
        HeroClass heroToCheck = null, bool invert = false)
    {
        //chooses targets
        GameObject cardTarget = cardPlayer1;
        GameObject otherGuy = cardPlayer2;
        GameObject panelTarget = playerPanel;
        GameObject panelOther = opponentPanel;
        if (target == "Opponent")
        {
            cardTarget = cardPlayer2;
            otherGuy = cardPlayer1;
            panelTarget = opponentPanel;
            panelOther = playerPanel;
        }

        //check the steal mana conditions for the player
        Vector2 stealMana = Vector2.zero;

        //if invert, should test against the other player card
        if (invert)
        {
            stealMana = CheckStealMana(targetAttacks, targetWins, otherGuy.GetComponent<CardController>().cardClass);
        }//otherwise, normal
        else
        {
            stealMana = CheckStealMana(targetAttacks, targetWins, cardTarget.GetComponent<CardController>().cardClass);
        }

        //steal mana
        if (stealMana.x > 0)
        {
            //take mana from the other player...
            ReduceMana(panelOther, (int)stealMana.x, (int)stealMana.y);

            //...and add to the target
            RecoverMana(panelTarget, (int)stealMana.x, (int)stealMana.y);
        }

        //same for hero
        if(heroToCheck != null)
        {
            stealMana = CheckStealMana(targetAttacks, targetWins, null, heroToCheck);
            if (stealMana.x > 0)
            {
                //take mana from the other player...
                ReduceMana(panelOther, (int)stealMana.x, (int)stealMana.y);

                //...and add to the target
                RecoverMana(panelTarget, (int)stealMana.x, (int)stealMana.y);
            }
        }
    }

    //steal life effect due to the steal life
    //target: which card is being tested (player of opponent)
    //bool parameters: indicates if target attacks and/or wins. Invert: when Copy Power is used
    private void StealLifeEffect(string target, bool targetAttacks, bool targetWins, GameObject cardPlayer1, GameObject cardPlayer2,
        bool invert = false)
    {
        //chooses targets
        GameObject cardTarget = cardPlayer1;
        GameObject otherGuy = cardPlayer2;
        GameObject panelTarget = playerPanel;
        GameObject panelOther = opponentPanel;
        if (target == "Opponent")
        {
            cardTarget = cardPlayer2;
            otherGuy = cardPlayer1;
            panelTarget = opponentPanel;
            panelOther = playerPanel;
        }

        //check the steal life conditions for the player
        Vector2 stealLife = CheckStealLife(cardTarget.GetComponent<CardController>(), targetAttacks, targetWins);

        //steal mana
        if (stealLife.x > 0)
        {
            //take mana from the other player...
            ReduceLife(panelOther, (int)stealLife.x, (int)stealLife.y);

            //...and add to the target
            RecoverLife(panelTarget, (int)stealLife.x, (int)stealLife.y);
        }
    }

    //check the Disable Power conditions
    private bool CheckDisablePower(CardController cardToCheck, bool isCardToCheckAttacking)
    {
        //should disable?
        bool shouldDisable = false;

        //see if the units have disable power effect
        List<EffectClass> effs = cardToCheck.cardClass.FindEffects("Disable Power");

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        shouldDisable = true;
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is a Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        shouldDisable = true;
                    }
                }//otherwise, no condition
                else
                {
                    shouldDisable = true;
                }
            }
        }

        return shouldDisable;
    }

    //check the Blank Status conditions
    private bool CheckBlankStatus(CardController cardToCheck, bool isCardToCheckAttacking)
    {
        //should blank?
        bool shouldBlank = false;

        //see if the units have blank status effect
        List<EffectClass> effs = cardToCheck.cardClass.FindEffects("Blank Status");

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        shouldBlank = true;
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is a Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        shouldBlank = true;
                    }
                }//otherwise, no condition
                else
                {
                    shouldBlank = true;
                }
            }
        }

        return shouldBlank;
    }

    //check the Copy Status conditions
    private bool CheckCopyStatus(CardController cardToCheck, bool isCardToCheckAttacking)
    {
        //should copy?
        bool shouldCopy = false;

        //see if the units have blank status effect
        List<EffectClass> effs = cardToCheck.cardClass.FindEffects("Copy Status");

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        shouldCopy = true;
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is a Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        shouldCopy = true;
                    }
                }//otherwise, no condition
                else
                {
                    shouldCopy = true;
                }
            }
        }

        return shouldCopy;
    }

    //check the Copy Power conditions
    private bool CheckCopyPower(CardController cardToCheck, bool isCardToCheckAttacking)
    {
        //should copy?
        bool shouldCopy = false;

        //see if the units have blank status effect
        List<EffectClass> effs = cardToCheck.cardClass.FindEffects("Copy Power");

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        shouldCopy = true;
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is a Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        shouldCopy = true;
                    }
                }//otherwise, no condition
                else
                {
                    shouldCopy = true;
                }
            }
        }

        return shouldCopy;
    }

    //check the Change Reinforcement conditions
    private EffectClass CheckChangeReinforcement(CardController cardToCheck, bool isCardToCheckAttacking)
    {
        //new reinforcement
        EffectClass newReinforcement = new EffectClass();

        //see if the units have blank status effect
        List<EffectClass> effs = cardToCheck.cardClass.FindEffects("Change Reinforcement");

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        newReinforcement = eff.GetReinforcement();
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is a Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        newReinforcement = eff.GetReinforcement();
                    }
                }//otherwise, no condition
                else
                {
                    newReinforcement = eff.GetReinforcement();
                }
            }
        }

        return newReinforcement;
    }

    //check the Tribal Force conditions
    private EffectClass CheckTribalForce(CardController cardToCheck, bool isCardToCheckAttacking)
    {
        //tribal Effect
        EffectClass tribalEffect = new EffectClass();

        //see if the units have blank status effect
        List<EffectClass> effs = cardToCheck.cardClass.FindEffects("Tribal Force");

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        tribalEffect = eff.GetReinforcement();
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is a Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        tribalEffect = eff.GetReinforcement();
                    }
                }//otherwise, no condition
                else
                {
                    tribalEffect = eff.GetReinforcement();
                }
            }
        }

        return tribalEffect;
    }

    //check the Status change conditions, both for cards and heroes
    private Vector2 CheckStatusChange(CardClass otherCard, bool isCardToCheckAttacking, CardClass cardToCheck = null, HeroClass heroToCheck = null)
    {
        //value to be added in attack/defense
        int value = 0;
        //int to identify if effect applies to Self or Opponent
        //0 = Self
        //1 = Opponent
        int target = 0;

        List<EffectClass> effs = new List<EffectClass>();
        //see if the units or hero have status change effect
        if (cardToCheck != null)
        {
            effs = cardToCheck.FindEffects("Status Change");
        }//else, hero
        else
        {
            effs = heroToCheck.FindEffects("Status Change");
        }

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        //check if there is any race against condition. 
                        //If the method return true, there is a satisfied condition or it is null. 
                        //If returns false, means there is a race restriction not applied
                        //same for vocation
                        bool raceCondition = false;
                        bool vocationCondition = false;

                        if (cardToCheck != null)
                        {
                            raceCondition = cardToCheck.CheckEffectRaceAgainst(eff, otherCard.GetRace().GetName());
                            vocationCondition = cardToCheck.CheckEffectVocationAgainst(eff, otherCard.GetVocation().GetName());
                        }//else, hero
                        else
                        {
                            raceCondition = heroToCheck.CheckEffectRaceAgainst(eff, otherCard.GetRace().GetName());
                            vocationCondition = heroToCheck.CheckEffectVocationAgainst(eff, otherCard.GetVocation().GetName());
                        }

                        //if againstRace is not null but the condition is valid, or if there is no againstRace rule
                        //same for vocation
                        if (((raceCondition && eff.GetAgainstRace() != null) || eff.GetAgainstRace() == null) &&
                            ((vocationCondition && eff.GetAgainstVocation() != null) || eff.GetAgainstVocation() == null))
                        {
                            //apply the modification
                            value += eff.GetAttackModifier();

                            //check if the effect is self or opponent
                            if(eff.GetTarget()[0] == "Opponent")
                            {
                                target = 1;
                            }
                        }
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is a Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        //check if there is any race against condition. 
                        //If the method return true, there is a satisfied condition or it is null. 
                        //If returns false, means there is a race restriction not applied
                        //same for vocation
                        bool raceCondition = false;
                        bool vocationCondition = false;

                        if (cardToCheck != null)
                        {
                            raceCondition = cardToCheck.CheckEffectRaceAgainst(eff, otherCard.GetRace().GetName());
                            vocationCondition = cardToCheck.CheckEffectVocationAgainst(eff, otherCard.GetVocation().GetName());
                        }//else, hero
                        else
                        {
                            raceCondition = heroToCheck.CheckEffectRaceAgainst(eff, otherCard.GetRace().GetName());
                            vocationCondition = heroToCheck.CheckEffectVocationAgainst(eff, otherCard.GetVocation().GetName());
                        }

                        //if againstRace is not null but the condition is valid, or if there is no againstRace rule
                        //same for vocation
                        if (((raceCondition && eff.GetAgainstRace() != null) || eff.GetAgainstRace() == null) &&
                            ((vocationCondition && eff.GetAgainstVocation() != null) || eff.GetAgainstVocation() == null))
                        {
                            //apply the modification
                            value += eff.GetAttackModifier();

                            //check if the effect is self or opponent
                            if (eff.GetTarget()[0] == "Opponent")
                            {
                                target = 1;
                            }
                        }
                    }
                }//otherwise, no condition
                else
                {
                    //check if there is any race against condition. 
                    //If the method return true, there is a satisfied condition or it is null. 
                    //If returns false, means there is a race restriction not applied
                    //same for vocation
                    bool raceCondition = false;
                    bool vocationCondition = false;

                    if (cardToCheck != null)
                    {
                        raceCondition = cardToCheck.CheckEffectRaceAgainst(eff, otherCard.GetRace().GetName());
                        vocationCondition = cardToCheck.CheckEffectVocationAgainst(eff, otherCard.GetVocation().GetName());
                    }//else, hero
                    else
                    {
                        raceCondition = heroToCheck.CheckEffectRaceAgainst(eff, otherCard.GetRace().GetName());
                        vocationCondition = heroToCheck.CheckEffectVocationAgainst(eff, otherCard.GetVocation().GetName());
                    }

                    //if againstRace is not null but the condition is valid, or if there is no againstRace rule
                    //same for vocation
                    if (((raceCondition && eff.GetAgainstRace() != null) || eff.GetAgainstRace() == null) &&
                        ((vocationCondition && eff.GetAgainstVocation() != null) || eff.GetAgainstVocation() == null))
                    {
                        //apply the modification
                        value += eff.GetAttackModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }
            }
        }

        return new Vector2(value, target);
    }

    //check the life change conditions
    private List<Vector2> CheckLifeChange(bool isCardToCheckAttacking, bool isCardToCheckWon, CardClass cardToCheck = null,
        HeroClass heroToCheck = null)
    {
        //value to be added in attack/defense
        int value = 0;
        //int to identify if effect applies to Self or Opponent
        //0 = Self
        //1 = Opponent
        int target = 0;
        //min and max modifiers
        Vector2 minMax = Vector2.zero;

        //see if the units have status change effect
        List<EffectClass> effs = new List<EffectClass>();
        if(cardToCheck != null)
        {
            effs = cardToCheck.FindEffects("Life Change");
        }
        else
        {
            effs = heroToCheck.FindEffects("Life Change");
        }

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is an Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }
                else if (eff.GetCondition() == "Win")
                {
                    //if there is an Win condition, just go on if this unit won
                    if (isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }
                else if (eff.GetCondition() == "Lose")
                {
                    //if there is an Lose condition, just go on if this unit lost
                    if (!isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }//otherwise, no condition
                else
                {
                    //apply the modification
                    value += eff.GetValueModifier();

                    //check if the effect is self or opponent
                    if (eff.GetTarget()[0] == "Opponent")
                    {
                        target = 1;
                    }
                }

                //set the min/max values
                minMax = new Vector2(eff.GetMinModifier(), eff.GetMaxModifier());
            }
        }

        return new List<Vector2> { new Vector2(value, target), minMax };
    }

    //check the mana change conditions
    private List<Vector2> CheckManaChange(bool isCardToCheckAttacking, bool isCardToCheckWon, CardClass cardToCheck = null, 
        HeroClass heroToCheck = null)
    {
        //value to be added in attack/defense
        int value = 0;
        //int to identify if effect applies to Self or Opponent
        //0 = Self
        //1 = Opponent
        int target = 0;
        //min and max modifiers
        Vector2 minMax = Vector2.zero;

        //see if the units have status change effect
        List<EffectClass> effs = new List<EffectClass>();

        if(cardToCheck != null)
        {
            effs = cardToCheck.FindEffects("Mana Change");
        }
        else
        {
            effs = heroToCheck.FindEffects("Mana Change");
        }

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is an Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }
                else if (eff.GetCondition() == "Win")
                {
                    //if there is an Win condition, just go on if this unit won
                    if (isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }
                else if (eff.GetCondition() == "Lose")
                {
                    //if there is an Lose condition, just go on if this unit lost
                    if (!isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();

                        //check if the effect is self or opponent
                        if (eff.GetTarget()[0] == "Opponent")
                        {
                            target = 1;
                        }
                    }
                }//otherwise, no condition
                else
                {
                    //apply the modification
                    value += eff.GetValueModifier();

                    //check if the effect is self or opponent
                    if (eff.GetTarget()[0] == "Opponent")
                    {
                        target = 1;
                    }
                }

                //set the min/max values
                minMax = new Vector2(eff.GetMinModifier(), eff.GetMaxModifier());
            }
        }
        
        return new List<Vector2> { new Vector2(value, target), minMax };
    }

    //check the steal mana conditions
    private Vector2 CheckStealMana(bool isCardToCheckAttacking, bool isCardToCheckWon, CardClass cardToCheck = null,
        HeroClass heroToCheck = null)
    {
        //value to be added in attack/defense
        int value = 0;
        //min modifier
        int min = 0;

        //see if the units have status change effect
        List<EffectClass> effs = new List<EffectClass>();
        if(cardToCheck != null)
        {
            effs = cardToCheck.FindEffects("Steal Mana");
        }
        else
        {
            effs = heroToCheck.FindEffects("Steal Mana");
        }

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is an Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }
                else if (eff.GetCondition() == "Win")
                {
                    //if there is an Win condition, just go on if this unit won
                    if (isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }
                else if (eff.GetCondition() == "Lose")
                {
                    //if there is an Lose condition, just go on if this unit lost
                    if (!isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }//otherwise, no condition
                else
                {
                    //apply the modification
                    value += eff.GetValueModifier();
                }

                //set the min/max values
                min = eff.GetMinModifier();
            }
        }

        return new Vector2(value, min);
    }

    //check the steal life conditions
    private Vector2 CheckStealLife(CardController cardToCheck, bool isCardToCheckAttacking, bool isCardToCheckWon)
    {
        //value to be added in attack/defense
        int value = 0;
        //min modifier
        int min = 0;

        //see if the units have status change effect
        List<EffectClass> effs = cardToCheck.cardClass.FindEffects("Steal Life");

        //for each effect found
        foreach (EffectClass eff in effs)
        {
            //if has, add its change
            if (eff.GetGeneralEffect() != "nope")
            {
                //check any condition effect (attack or defend)
                if (eff.GetCondition() == "Attack")
                {
                    //if there is an Attack condition, just go on if this unit is attacking
                    if (isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }
                else if (eff.GetCondition() == "Defend")
                {
                    //if there is an Defend condition, just go on if this unit is not attacking
                    if (!isCardToCheckAttacking)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }
                else if (eff.GetCondition() == "Win")
                {
                    //if there is an Win condition, just go on if this unit won
                    if (isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }
                else if (eff.GetCondition() == "Lose")
                {
                    //if there is an Lose condition, just go on if this unit lost
                    if (!isCardToCheckWon)
                    {
                        //apply the modification
                        value += eff.GetValueModifier();
                    }
                }//otherwise, no condition
                else
                {
                    //apply the modification
                    value += eff.GetValueModifier();
                }

                //set the min/max values
                min = eff.GetMinModifier();
            }
        }

        return new Vector2(value, min);
    }

    //diminish life
    private void ReduceLife(GameObject target, int lifeLost, int qntMin)
    {
        //absolute damage
        lifeLost = Mathf.Abs(lifeLost);

        //check if does not pass the min value, and diminish it
        if (target.name == "PlayerPanel")
        {
            while ((playerHealth - lifeLost) < qntMin)
            {
                lifeLost--;
            }

            playerHealth -= lifeLost;
        }
        else
        {   
            while ((opponentHealth - lifeLost) < qntMin)
            {
                lifeLost--;
            }

            opponentHealth -= lifeLost;
        }

        //diminish the life in the board
        int life = target.transform.Find("LifeBar").childCount;
        for (int i = life - 1; i >= life - lifeLost; i--)
        {
            //check if life lost is more than the remaining life
            if(i < 0)
            {
                break;
            }

            GameObject.Destroy(target.transform.Find("LifeBar").GetChild(i).gameObject);
        }
    }

    //diminish mana
    private void ReduceMana(GameObject target, int manaLost, int qntMin)
    {
        //absolute damage
        manaLost = Mathf.Abs(manaLost);

        //check if does not pass the min value, and diminish it
        if(target.name == "PlayerPanel")
        {
            while((playerMana - manaLost) < qntMin)
            {
                manaLost--;
            }

            playerMana -= manaLost;
        }
        else
        {
            while ((opponentMana - manaLost) < qntMin)
            {
                manaLost--;
            }

            opponentMana -= manaLost;
        }

        //diminish the mana in the board
        int mana = target.transform.Find("ManaBar").childCount;
        for (int i = mana - 1; i >= mana - manaLost; i--)
        {
            //check if mana lost is more than the remaining life
            if (i < 0)
            {
                break;
            }

            DestroyImmediate(target.transform.Find("ManaBar").GetChild(i).gameObject);
        }
    }

    //recover mana
    private void RecoverMana(GameObject target, int qntToAdd, int qntMax)
    {
        //check if does not pass the max value
        if (target.name == "PlayerPanel")
        {
            //just check if there is a max value
            if (qntMax > 0)
            {
                while ((playerMana + qntToAdd) > qntMax)
                {
                    qntToAdd--;
                }
            }
        }
        else
        {
            //just check if there is a max value
            if (qntMax > 0)
            {
                while ((opponentMana + qntToAdd) > qntMax)
                {
                    qntToAdd--;
                }
            }
        }

        //foreach qnt to add
        for (int i = 0; i < qntToAdd; i++)
        {
            int manas = target.transform.Find("ManaBar").childCount;
            GameObject newMana = Instantiate(manaPF);
            //parent
            newMana.transform.SetParent(target.transform.Find("ManaBar").transform);
            //rect transform
            Vector3 initialPos = Vector3.zero;
            if (manas > 0)
            {
                //position can be in second line also
                if (manas == 10)
                {
                    initialPos = target.transform.Find("ManaBar").GetChild(0).GetComponent<RectTransform>().anchoredPosition;
                    initialPos -= new Vector3(28, 28, 0);
                }
                else
                {
                    initialPos = target.transform.Find("ManaBar").GetChild(manas - 1).GetComponent<RectTransform>().anchoredPosition;
                }
                
                //update pos
                initialPos = new Vector3(initialPos.x + 28, initialPos.y, initialPos.z);
            }
            else
            {
                initialPos = manaPF.GetComponent<RectTransform>().anchoredPosition;
            }
            newMana.GetComponent<RectTransform>().anchoredPosition = initialPos;

            newMana.GetComponent<RectTransform>().localScale = manaPF.GetComponent<RectTransform>().localScale;
            //name
            newMana.name = "Mana_" + manas;
        }

        //raise mana
        if(target.name == "PlayerPanel")
        {
            playerMana += qntToAdd;
        }
        else
        {
            opponentMana += qntToAdd;
        }
    }

    //recover life
    private void RecoverLife(GameObject target, int qntToAdd, int qntMax)
    {
        //check if does not pass the max value
        if (target.name == "PlayerPanel")
        {
            //just check if there is a max value
            if (qntMax > 0)
            {
                while ((playerHealth + qntToAdd) > qntMax)
                {
                    qntToAdd--;
                }
            }
        }
        else
        {
            //just check if there is a max value
            if (qntMax > 0)
            {
                while ((opponentHealth + qntToAdd) > qntMax)
                {
                    qntToAdd--;
                }
            }
        }

        //foreach qnt to add
        for (int i = 0; i < qntToAdd; i++)
        {
            int life = target.transform.Find("LifeBar").childCount;
            GameObject newLife = Instantiate(healthPF);
            //parent
            newLife.transform.SetParent(target.transform.Find("LifeBar").transform);
            //rect transform
            Vector3 initialPos = Vector3.zero;
            if (life > 0)
            {
                //position can be in second line also
                if(life == 10)
                {
                    initialPos = target.transform.Find("LifeBar").GetChild(0).GetComponent<RectTransform>().anchoredPosition;
                    initialPos -= new Vector3(28, 28, 0);
                }
                else
                {
                    initialPos = target.transform.Find("LifeBar").GetChild(life - 1).GetComponent<RectTransform>().anchoredPosition;
                }

                //update pos
                initialPos = new Vector3(initialPos.x + 28, initialPos.y, initialPos.z);
            }
            else
            {
                initialPos = healthPF.GetComponent<RectTransform>().anchoredPosition;
            }
            newLife.GetComponent<RectTransform>().anchoredPosition = initialPos;

            newLife.GetComponent<RectTransform>().localScale = healthPF.GetComponent<RectTransform>().localScale;
            //name
            newLife.name = "Health_" + life;
        }

        //raise life
        if (target.name == "PlayerPanel")
        {
            playerHealth += qntToAdd;
        }
        else
        {
            opponentHealth += qntToAdd;
        }
    }

    //game over

    //save database info (not used in the game itself)
    private void SaveInitialDatabase()
    {
        //all possible vocations
        List<VocationClass> allVocations = new List<VocationClass>();
        allVocations.Add(new VocationClass("Warrior"));
        allVocations.Add(new VocationClass("Mage"));
        allVocations.Add(new VocationClass("Barbarian"));
        allVocations.Add(new VocationClass("Archer"));
        allVocations.Add(new VocationClass("Guardian"));
        allVocations.Add(new VocationClass("Ladin"));
        allVocations.Add(new VocationClass("Healer"));

        //all possible races
        List<RaceClass> allRaces = new List<RaceClass>();
        allRaces.Add(new RaceClass("Orc"));
        allRaces.Add(new RaceClass("Dwarf"));
        allRaces.Add(new RaceClass("Elf"));
        allRaces.Add(new RaceClass("Human"));

        //all possible heroes
        List<HeroClass> allHeroes = new List<HeroClass>();
        //0 - Trosh
        allHeroes.Add(new HeroClass("Trosh", allRaces[0], allVocations[2], new List<EffectClass> {
            new EffectClass("Status Change", "All your Orcs receive +1/+0", "", "",
            new List<string> { "Orc" }, 1, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "All your Orcs receive +1/+0 against Mage", "", "",
            new List<string> { "Orc" }, 1, 0, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[1] }, null, null)
        }));
        //1 - Ahkson, The Black Shaman
        allHeroes.Add(new HeroClass("Ahkson, The Black Shaman", allRaces[0], allVocations[1], new List<EffectClass> {
            new EffectClass("Status Change", "All your Human opponents receive -1/-0", "", "",
            new List<string> { "Opponent" }, -1, 0, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[3] }, null, null, null),
            new EffectClass("Mana Change", "Each turn, opponent loses 1 mana, min 2", "Mana", "Turn",
            new List<string> { "Opponent" }, 0, 0, -1, 0, 2, null, null, null, null, null, null)
        }));
        //2 - Bolin, The Defender
        allHeroes.Add(new HeroClass("Bolin, The Defender", allRaces[1], allVocations[4], new List<EffectClass> {
            new EffectClass("Status Change", "All your Dwarves receive +0/+1", "", "",
            new List<string> { "Dwarf" }, 0, 1, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "All your Guardians receive +1/+0", "", "",
            new List<string> { "Guardian" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //3 - Tolin, The Light Paladin
        allHeroes.Add(new HeroClass("Tolin, The Light Paladin", allRaces[1], allVocations[0], new List<EffectClass> {
            new EffectClass("Status Change", "All your Dwarves receive +1/+0", "", "",
            new List<string> { "Dwarf" }, 1, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "All your Warriors receive +0/+1", "", "",
            new List<string> { "Warrior" }, 0, 1, 0, 0, 0, null, null, null, null, null, null)
        }));
        //4 - Galthoniel, The Bowsinger
        allHeroes.Add(new HeroClass("Galthoniel, The Bowsinger", allRaces[2], allVocations[3], new List<EffectClass> {
            new EffectClass("Status Change", "All your Archers receive +1/+0", "", "",
            new List<string> { "Archer" }, 1, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "All your Archers receive +1/+0 against Barbarian", "", "",
            new List<string> { "Archer" }, 1, 0, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[2] }, null, null)
        }));
        //5 - Tumissa, The Lady of Forest
        allHeroes.Add(new HeroClass("Tumissa, The Lady of Forest", allRaces[2], allVocations[1], new List<EffectClass> {
            new EffectClass("Status Change", "All your Elves receive +0/+1", "", "",
            new List<string> { "Elf" }, 0, 1, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Mana Change", "Each turn, gains 1 mana, max 13", "Mana", "Turn",
            new List<string> { "Self" }, 0, 0, 1, 13, 0, null, null, null, null, null, null)
        }));
        //6 - Baladir, The Swordmaster
        allHeroes.Add(new HeroClass("Baladir, The Swordmaster", allRaces[3], allVocations[0], new List<EffectClass> {
            new EffectClass("Status Change", "All your Humans receive +1/+0", "", "",
            new List<string> { "Human" }, 1, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "All your Warriors receive +0/+1", "", "",
            new List<string> { "Warrior" }, 0, 1, 0, 0, 0, null, null, null, null, null, null)
        }));
        //7 - Radir, The Shadow
        allHeroes.Add(new HeroClass("Radir, The Shadow", allRaces[3], allVocations[5], new List<EffectClass> {
            new EffectClass("Status Change", "All your Humans receive +0/+1", "", "",
            new List<string> { "Human" }, 0, 1, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Steal Mana", "All your Ladins steal 1 mana if win", "Mana", "Win",
            new List<string> { "Ladin" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));

        //all possible cards
        List<CardClass> allCards = new List<CardClass>();
        //ORCS!!!
        //0 - Light Shaman
        allCards.Add(new CardClass("Light Shaman", "Common", allRaces[0], allVocations[1], 1, 2, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If defends against Mage or Archer, receives +0/+1", "", "Defend", 
            new List<string> { "Self" }, 0, 1, 0, 0, 0, null, null, null, new List<VocationClass>{
            allVocations[1], allVocations[3]}, null, null)
        }));
        //1 - Bogus
        allCards.Add(new CardClass("Bogus", "Common", allRaces[0], allVocations[5], 1, 1, 1, new List<EffectClass> {
            new EffectClass("Steal Mana", "If attacks, steals 1 mana", "Mana", "Attack",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Steal Mana", "If wins, steals 1 mana", "Mana", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //2 - Orcish Bow
        allCards.Add(new CardClass("Orcish Bow", "Common", allRaces[0], allVocations[3], 2, 1, 1, null));
        //3 - Orc Soldier
        allCards.Add(new CardClass("Orc Soldier", "Common", allRaces[0], allVocations[0], 1, 1, 1, new List<EffectClass> {
            new EffectClass("Status Change", "Receives +1/+1 against Dwarves", "", "",
            new List<string> { "Self" }, 1, 1, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[1] }, null, null, null)
        }));
        //4 - The Crazy Shaman
        allCards.Add(new CardClass("The Crazy Shaman", "Common", allRaces[0], allVocations[1], 1, 0, 1, new List<EffectClass> {
            new EffectClass("Disable Power", "If attacks, disable power", "", "Attack",
            new List<string> { "Opponent" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //5 - The Kamikaze Orc
        allCards.Add(new CardClass("The Kamikaze Orc", "Common", allRaces[0], allVocations[2], 2, 0, 1, new List<EffectClass> {
            new EffectClass("Initiative", "If in the hand, need to be played", "", "",
            new List<string> { "Self" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //6 - Warrior Orc
        allCards.Add(new CardClass("Warrior Orc", "Common", allRaces[0], allVocations[0], 1, 2, 1, null));
        //7 - Orc Doctor
        allCards.Add(new CardClass("Orc Doctor", "Common", allRaces[0], allVocations[6], 0, 1, 1, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 2 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 2, 0, 0, null, null, null, null, null, null)
        }));
        //8 - Xenophobic Orc
        allCards.Add(new CardClass("Xenophobic Orc", "Common", allRaces[0], allVocations[0], 3, 1, 2, new List<EffectClass> {
            new EffectClass("Status Change", "if attacks against Orc, receives -2/-0", "", "Attack",
            new List<string> { "Self" }, -2, 0, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[0] }, null, null, null),
            new EffectClass("Status Change", "If attacks against non-Orc, receives +2/+0", "", "Attack",
            new List<string> { "Self" }, +2, 0, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[1],allRaces[2], allRaces[3] }, 
            null, null, null)
        }));
        //9 - The Old Shaman
        allCards.Add(new CardClass("The Old Shaman", "Uncommon", allRaces[0], allVocations[6], 0, 3, 2, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 1 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Life Change", "If wins, gains 1 life", "Life", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //10 - Thrall, The Orc Shield
        allCards.Add(new CardClass("Thrall, The Orc Shield", "Common", allRaces[0], allVocations[4], 0, 5, 2, null));
        //11 - Siege Leader
        allCards.Add(new CardClass("Siege Leader", "Common", allRaces[0], allVocations[3], 3, 1, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //12 - Mhhaaark
        allCards.Add(new CardClass("Mhhaaark", "Common", allRaces[0], allVocations[2], 1, 3, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If defends against Human or Elf, receives +0/+2", "", "Defend",
            new List<string> { "Self" }, 0, 2, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[2], allRaces[3] }, null, null, null)
        }));
        //13 - Mimic Orc
        allCards.Add(new CardClass("Mimic Orc", "Uncommon", allRaces[0], allVocations[1], 0, 0, 2, new List<EffectClass> {
            new EffectClass("Copy Status", "Copy the opponent status", "", "",
            new List<string> { "Self" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //14 - Frenetic Barbarian
        allCards.Add(new CardClass("Frenetic Barbarian", "Uncommon", allRaces[0], allVocations[2], 4, 3, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //15 - The Reaper
        allCards.Add(new CardClass("The Reaper", "Uncommon", allRaces[0], allVocations[1], 1, 3, 3, new List<EffectClass> {
            new EffectClass("Change Reinforcement", "If defends, for each aditional mana used -1 adversary life min 1, instead +1/+1", 
            "", "Defend", new List<string> { "Opponent" }, 0, 0, 0, 0, 0, new EffectClass( "Life Change", 
            "Change Reinforcement -1 life min 1", "Life", "Defend", new List<string> { "Opponent" }, 0, 0, -1, 0, 1, 
            null, null, null, null, null, null), null, null, null, null, null)
        }));
        //16 - Blomb, The Orc Leader
        allCards.Add(new CardClass("Blomb, The Orc Leader", "Common", allRaces[0], allVocations[0], 4, 1, 3, null));
        //17 - Groroth, The Elfbreaker
        allCards.Add(new CardClass("Groroth, The Elfbreaker", "Uncommon", allRaces[0], allVocations[2], 2, 2, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks against Elf, receives +2/+0", "", "Attack",
            new List<string> { "Self" }, 2, 0, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[2] }, null, null, null)
        }));
        //18 - Hrash, The human-eater
        allCards.Add(new CardClass("Hrash, The human-eater", "Rare", allRaces[0], allVocations[2], 4, 1, 4, new List<EffectClass> {
            new EffectClass("Tribal Force", "Receives +1/+1 for each Human in the game", "", "",
            new List<string> { "Self" }, 1, 1, 0, 0, 0, new EffectClass("Status Change", "Tribal Force +1/+1 vs Humans", 
            "", "", new List<string> { "Self" }, 1, 1, 0, 0, 0, null, null, null, null, new List<RaceClass>{ allRaces[3] }, null
            ), null, null, null, new List<RaceClass>{ allRaces[3] }, null)
        }));
        //19 - Momor, The Ahkson Acolyte
        allCards.Add(new CardClass("Momor, The Ahkson Acolyte", "Rare", allRaces[0], allVocations[1], 1, 4, 4, new List<EffectClass> {
            new EffectClass("Mana Change", "If defends, opponent loses 2 mana", "Mana", "Defend",
            new List<string> { "Opponent" }, 0, 0, -2, 0, 0, null, null, null, null, null, null),
            new EffectClass("Mana Change", "If wins, opponent loses 3 mana", "Mana", "Win",
            new List<string> { "Opponent" }, 0, 0, -3, 0, 0, null, null, null, null, null, null)
        }));

        //DWARVES!!!
        //20 - Mine Dwarf
        allCards.Add(new CardClass("Mine Dwarf", "Common", allRaces[1], allVocations[0], 1, 2, 1, null));
        //21 - Dwarf Warden
        allCards.Add(new CardClass("Dwarf Warden", "Common", allRaces[1], allVocations[0], 2, 1, 1, null));
        //22 - Outlander
        allCards.Add(new CardClass("Outlander", "Common", allRaces[1], allVocations[2], 3, 0, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks against Mage, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, new List<VocationClass>{
            allVocations[1]}, null, null)
        }));
        //23 - The Crafter
        allCards.Add(new CardClass("The Crafter", "Common", allRaces[1], allVocations[1], 0, 2, 1, new List<EffectClass> {
            new EffectClass("Mana Change", "If defends, gains 1 mana", "Mana", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //24 - Dwarf Army
        allCards.Add(new CardClass("Dwarf Army", "Common", allRaces[1], allVocations[0], 1, 1, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //25 - Dwarf Cleric
        allCards.Add(new CardClass("Dwarf Cleric", "Common", allRaces[1], allVocations[6], 0, 2, 1, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 1 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //26 - The Tiny Runaway
        allCards.Add(new CardClass("The Tiny Runaway", "Uncommon", allRaces[1], allVocations[5], 3, 1, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If there is any Dwarf in game, receives -2/-0", "", "",
            new List<string> { "Self" }, -2, 0, 0, 0, 0, null, null, null, null, new List<RaceClass>{ allRaces[1] }, null)
        }));
        //27 - Foreman Dwarf
        allCards.Add(new CardClass("Foreman Dwarf", "Common", allRaces[1], allVocations[4], 1, 2, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks against Orc, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[0] }, null, null, null)
        }));
        //28 - Lont Apprentice
        allCards.Add(new CardClass("Lont Apprentice", "Common", allRaces[1], allVocations[1], 0, 1, 2, new List<EffectClass> {
            new EffectClass("Disable Power", "If defends, disable power", "", "Defend",
            new List<string> { "Opponent" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //29 - The Orc hunter
        allCards.Add(new CardClass("The Orc Hunter", "Common", allRaces[1], allVocations[4], 3, 2, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks against Orc, receives +2/+0", "", "Attack",
            new List<string> { "Self" }, 2, 0, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[0] }, null, null, null)
        }));
        //30 - The Flak Dwarf
        allCards.Add(new CardClass("The Flak Dwarf", "Common", allRaces[1], allVocations[0], 2, 2, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If fights against Archer, receives +1/+1", "", "",
            new List<string> { "Self" }, 1, 1, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[3] }, null, null)
        }));
        //31 - Dwarf Jeweler
        allCards.Add(new CardClass("Dwarf Jeweler", "Uncommon", allRaces[1], allVocations[1], 1, 3, 2, new List<EffectClass> {
            new EffectClass("Mana Change", "If defends, gains 1 mana", "Mana", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Mana Change", "If wins, gains 1 mana", "Mana", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //32 - Buorlin, The Elf Friend
        allCards.Add(new CardClass("Buorlin, The Elf Friend", "Common", allRaces[1], allVocations[0], 3, 2, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If there is any Elf in game, receives +1/+1", "", "",
            new List<string> { "Self" }, 1, 1, 0, 0, 0, null, null, null, null, new List<RaceClass>{ allRaces[2] }, null)
        }));
        //33 - Muor, The Champion
        allCards.Add(new CardClass("Muor, The Champion", "Common", allRaces[1], allVocations[0], 3, 1, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If fights against Archer or Warrior, receives +2/+0", "", "",
            new List<string> { "Self" }, 2, 0, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[0], allVocations[3] }, 
            null, null)
        }));
        //34 - Lont, The Canceller
        allCards.Add(new CardClass("Lont, The Canceller", "Common", allRaces[1], allVocations[1], 2, 3, 3, new List<EffectClass> {
            new EffectClass("Disable Power", "Disable power", "", "",
            new List<string> { "Opponent" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //35 - Gili, The Protector
        allCards.Add(new CardClass("Gili, The Protector", "Uncommon", allRaces[1], allVocations[4], 2, 5, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If defends, receives +0/+1", "", "Defend",
            new List<string> { "Self" }, 0, 1, 0, 0, 0, null, null, null, null, null, null)
        }));
        //36 - Fulin, The Peacemaker
        allCards.Add(new CardClass("Fulin, The Peacemaker", "Uncommon", allRaces[1], allVocations[6], 0, 0, 3, new List<EffectClass> {
            new EffectClass("Blank Status", "Opponent status are zeroed", "", "",
            new List<string> { "Opponent" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //37 - Minara, The Old Lady
        allCards.Add(new CardClass("Minara, The Old Lady", "Uncommon", allRaces[1], allVocations[6], 0, 2, 3, new List<EffectClass> {
            new EffectClass("Tribal Force", "If wins, gains 1 life for each Dwarf in the game", "Life", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, new EffectClass("Life Change", "Tribal Force +1 Life",
            "Life", "Win", new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, new List<RaceClass>{ allRaces[1] }, null
            ), null, null, null, new List<RaceClass>{ allRaces[1] }, null)
        }));
        //38 - Murolin, The Wall Scout
        allCards.Add(new CardClass("Murolin, The Wall Scout", "Rare", allRaces[1], allVocations[4], 1, 4, 4, new List<EffectClass> {
            new EffectClass("Change Reinforcement", "If defends, for each aditional mana used +0/+2, instead +1/+1",
            "", "Defend", new List<string> { "Self" }, 0, 0, 0, 0, 0, new EffectClass( "Status Change",
            "Change Reinforcement +0/+2", "", "Defend", new List<string> { "Self" }, 0, 2, 0, 0, 0,
            null, null, null, null, null, null), null, null, null, null, null)
        }));
        //39 - Olvidin, The Golden King
        allCards.Add(new CardClass("Olvidin, The Golden King", "Rare", allRaces[1], allVocations[1], 1, 3, 4, new List<EffectClass> {
            new EffectClass("Tribal Force", "Gains 1 mana for each Dwarf in the game", "Mana", "",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, new EffectClass("Mana Change", "Tribal Force +1 Mana",
            "Mana", "", new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, new List<RaceClass>{ allRaces[1] }, null
            ), null, null, null, new List<RaceClass>{ allRaces[1] }, null)
        }));

        //ELVES!!!
        //40 - Elf Scout
        allCards.Add(new CardClass("Elf Scout", "Common", allRaces[2], allVocations[3], 2, 1, 1, null));
        //41 - Elf Warrior
        allCards.Add(new CardClass("Elf Warrior", "Common", allRaces[2], allVocations[0], 1, 2, 1, null));
        //42 - Elvish Sage
        allCards.Add(new CardClass("Elvish Sage", "Common", allRaces[2], allVocations[1], 1, 2, 1, new List<EffectClass> {
            new EffectClass("Mana Change", "If defends, gains 1 mana", "Mana", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //43 - Elvish Druid
        allCards.Add(new CardClass("Elvish Druid", "Common", allRaces[2], allVocations[6], 0, 2, 1, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 1 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //44 - Wood Ranger
        allCards.Add(new CardClass("Wood Ranger", "Common", allRaces[2], allVocations[0], 1, 1, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If fights against Orc, receives +1/+1", "", "",
            new List<string> { "Self" }, 1, 1, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[0] }, null, null, null)
        }));
        //45 - Harmless Elf
        allCards.Add(new CardClass("Harmless Elf", "Common", allRaces[2], allVocations[0], 0, 1, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If defends, opponent receives -2/-0", "", "Defend",
            new List<string> { "Opponent" }, -2, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //46 - Tellarius, Magebreaker
        allCards.Add(new CardClass("Tellarius, Magebreaker", "Common", allRaces[2], allVocations[3], 2, 0, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks against Mage or Healer, receives +2/+0", "", "Attack",
            new List<string> { "Self" }, 2, 0, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[1], allVocations[6] }, 
            null, null)
        }));
        //47 - Dallius
        allCards.Add(new CardClass("Dallius", "Common", allRaces[2], allVocations[1], 0, 2, 1, new List<EffectClass> {
            new EffectClass("Disable Power", "If defends, disable power", "", "Defend",
            new List<string> { "Opponent" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //48 - Glannil
        allCards.Add(new CardClass("Glannil", "Common", allRaces[2], allVocations[3], 4, 1, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //49 - Thrillin, Orcbane
        allCards.Add(new CardClass("Thrillin, Orcbane", "Common", allRaces[2], allVocations[3], 3, 2, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks agains Orc or Warrior, receives +2/+0", "", "Attack",
            new List<string> { "Self" }, 2, 0, 0, 0, 0, null, null, new List<RaceClass>{ allRaces[0] }, 
            new List<VocationClass>{ allVocations[0] }, null, null)
        }));
        //50 - Acheron
        allCards.Add(new CardClass("Acheron", "Uncommon", allRaces[2], allVocations[1], 2, 3, 2, new List<EffectClass> {
            new EffectClass("Mana Change", "If defends, gains 1 mana", "Mana", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //51 - Greenleaf
        allCards.Add(new CardClass("Greenleaf", "Common", allRaces[2], allVocations[6], 1, 3, 2, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 1 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Life Change", "If wins, gains 1 life", "Life", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //52 - Zarallonis, Shieldbreaker
        allCards.Add(new CardClass("Zarallonis, Shieldbreaker", "Common", allRaces[2], allVocations[0], 2, 2, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, opponent receives -0/-2", "", "Attack",
            new List<string> { "Opponent" }, 0, -2, 0, 0, 0, null, null, null, null, null, null)
        }));
        //53 - Singlinn, Shapeshifter
        allCards.Add(new CardClass("Singlinn, Shapeshifter", "Uncommon", allRaces[2], allVocations[1], 0, 0, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +3/+0", "", "Attack",
            new List<string> { "Self" }, 3, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "If defends, receives +0/+3", "", "Defend",
            new List<string> { "Self" }, 0, 3, 0, 0, 0, null, null, null, null, null, null)
        }));
        //54 - Glingant
        allCards.Add(new CardClass("Glingant", "Uncommon", allRaces[2], allVocations[0], 4, 3, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "If defends, receives +0/+1", "", "Defend",
            new List<string> { "Self" }, 0, 1, 0, 0, 0, null, null, null, null, null, null)
        }));
        //55 - Elite Archer
        allCards.Add(new CardClass("Elite Archer", "Uncommon", allRaces[2], allVocations[3], 5, 2, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //56 - Hrigan
        allCards.Add(new CardClass("Hrigan", "Common", allRaces[2], allVocations[1], 3, 4, 3, new List<EffectClass> {
            new EffectClass("Mana Change", "If attacks, gains 1 mana", "Mana", "Attack",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Mana Change", "If wins, gains 1 mana", "Mana", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //57 - Danarglin
        allCards.Add(new CardClass("Danarglin", "Uncommon", allRaces[2], allVocations[6], 0, 5, 3, new List<EffectClass> {
            new EffectClass("Change Reinforcement", "If attacks, for each aditional mana used gains 1 life, instead +1/+1",
            "", "Attack", new List<string> { "Self" }, 0, 0, 0, 0, 0, new EffectClass( "Life Change",
            "Change Reinforcement +1 life", "", "Attack", new List<string> { "Self" }, 0, 0, 1, 0, 0,
            null, null, null, null, null, null), null, null, null, null, null)
        }));
        //58 - Thriliriel, Bane of Shadows
        allCards.Add(new CardClass("Thriliriel, Bane of Shadows", "Rare", allRaces[2], allVocations[3], 4, 1, 4, new List<EffectClass> {
            new EffectClass("Change Reinforcement", "If attacks, for each aditional mana used +2/+0, instead +1/+1",
            "", "Attack", new List<string> { "Self" }, 0, 0, 0, 0, 0, new EffectClass( "Status Change",
            "Change Reinforcement +2/+0", "", "Attack", new List<string> { "Self" }, 2, 0, 0, 0, 0,
            null, null, null, null, null, null), null, null, null, null, null)
        }));
        //59 - Sarillin, Beacon of Hope
        allCards.Add(new CardClass("Sarillin, Beacon of Hope", "Rare", allRaces[2], allVocations[6], 2, 5, 4, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 2 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 2, 0, 0, null, null, null, null, null, null),
            new EffectClass("Life Change", "If wins, gains 3 life", "Life", "Win",
            new List<string> { "Self" }, 0, 0, 3, 0, 0, null, null, null, null, null, null)
        }));

        //HUMANS!!!
        //60 - Human Scout
        allCards.Add(new CardClass("Human Scout", "Common", allRaces[3], allVocations[0], 2, 1, 1, null));
        //61 - Human Defender
        allCards.Add(new CardClass("Human Defender", "Common", allRaces[3], allVocations[4], 0, 3, 1, null));
        //62 - Human Archer
        allCards.Add(new CardClass("Human Archer", "Common", allRaces[3], allVocations[3], 2, 1, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks against Mage or Healer, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[1], allVocations[6] },
            null, null)
        }));
        //63 - Herbalist
        allCards.Add(new CardClass("Herbalist", "Common", allRaces[3], allVocations[6], 0, 2, 1, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 1 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //64 - Arcane
        allCards.Add(new CardClass("Arcane", "Common", allRaces[3], allVocations[1], 1, 1, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "If defends, receives +0/+1", "", "Defend",
            new List<string> { "Self" }, 0, 1, 0, 0, 0, null, null, null, null, null, null)
        }));
        //65 - Crazy Man
        allCards.Add(new CardClass("Crazy Man", "Common", allRaces[3], allVocations[2], 2, 0, 1, new List<EffectClass> {
            new EffectClass("Initiative", "If in the hand, need to be played", "", "",
            new List<string> { "Self" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //66 - Tricky
        allCards.Add(new CardClass("Tricky", "Common", allRaces[3], allVocations[5], 1, 1, 1, new List<EffectClass> {
            new EffectClass("Steal Mana", "If attacks, steals 1 mana", "Mana", "Attack",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Steal Mana", "If wins, steals 1 mana", "Mana", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //67 - Frontline Soldier
        allCards.Add(new CardClass("Frontline Soldier", "Common", allRaces[3], allVocations[0], 1, 2, 1, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, opponent receives -0/-1", "", "Attack",
            new List<string> { "Opponent" }, 0, -1, 0, 0, 0, null, null, null, null, null, null)
        }));
        //68 - Bardin
        allCards.Add(new CardClass("Bardin", "Common", allRaces[3], allVocations[3], 3, 1, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +2/+0", "", "Attack",
            new List<string> { "Self" }, 2, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //69 - Dollus
        allCards.Add(new CardClass("Dollus", "Common", allRaces[3], allVocations[4], 1, 4, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If defends against Archer, receives +0/+2", "", "Defend",
            new List<string> { "Self" }, 0, 2, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[3] }, null, null)
        }));
        //70 - An, The Ritualist
        allCards.Add(new CardClass("An, The Ritualist", "Common", allRaces[3], allVocations[1], 2, 2, 2, new List<EffectClass> {
            new EffectClass("Disable Power", "If defends, disable power", "", "Defend",
            new List<string> { "Opponent" }, 0, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //71 - Marlisa
        allCards.Add(new CardClass("Marlisa", "Uncommon", allRaces[3], allVocations[6], 1, 3, 2, new List<EffectClass> {
            new EffectClass("Life Change", "If defends, gains 1 life", "Life", "Defend",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Life Change", "If wins, gains 1 life", "Life", "Win",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //72 - Drodt, Hammerbearer
        allCards.Add(new CardClass("Drodt, Hammerbearer", "Common", allRaces[3], allVocations[0], 3, 2, 2, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks against Guardian, receives +2/+0", "", "Attack",
            new List<string> { "Self" }, 2, 0, 0, 0, 0, null, null, null, new List<VocationClass>{ allVocations[4] }, null, null)
        }));
        //73 - Mirt, Plainhidden
        allCards.Add(new CardClass("Mirt, Plainhidden", "Uncommon", allRaces[3], allVocations[5], 4, 1, 2, new List<EffectClass> {
            new EffectClass("Steal Mana", "If attacks, steals 1 mana", "Mana", "Attack",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null),
            new EffectClass("Steal Mana", "If loses, steals 1 mana", "Mana", "Lose",
            new List<string> { "Self" }, 0, 0, 1, 0, 0, null, null, null, null, null, null)
        }));
        //74 - Brativos
        allCards.Add(new CardClass("Brativos", "Uncommon", allRaces[3], allVocations[2], 5, 2, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Status Change", "If defends, receives +0/+1", "", "Defend",
            new List<string> { "Self" }, 0, 1, 0, 0, 0, null, null, null, null, null, null)
        }));
        //75 - Gormund
        allCards.Add(new CardClass("Gormund", "Common", allRaces[3], allVocations[0], 4, 3, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If attacks, receives +1/+0", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, null)
        }));
        //76 - Craniack, Weird Shaman
        allCards.Add(new CardClass("Craniack, Weird Shaman", "Uncommon", allRaces[3], allVocations[1], 3, 3, 3, new List<EffectClass> {
            new EffectClass("Mana Change", "If attacks, opponent loses 2 mana, min 1", "Mana", "Attack",
            new List<string> { "Opponent" }, 0, 0, -2, 0, 1, null, null, null, null, null, null),
            new EffectClass("Mana Change", "If loses, opponent loses 1 mana, min 1", "Mana", "Lose",
            new List<string> { "Opponent" }, 0, 0, -1, 0, 1, null, null, null, null, null, null)
        }));
        //77 - Gront, The Blockade
        allCards.Add(new CardClass("Gront, The Blockade", "Uncommon", allRaces[3], allVocations[4], 1, 5, 3, new List<EffectClass> {
            new EffectClass("Status Change", "If defends, receives +0/+2", "", "Defend",
            new List<string> { "Self" }, 0, 2, 0, 0, 0, null, null, null, null, null, null)
        }));
        //78 - Brutus
        allCards.Add(new CardClass("Brutus", "Rare", allRaces[3], allVocations[0], 3, 4, 4, new List<EffectClass> {
            new EffectClass("Tribal Force", "If attacks, gains +1/+0 for each Warrior in the game", "", "Attack",
            new List<string> { "Self" }, 1, 0, 0, 0, 0, new EffectClass("Status Change", "Tribal Force +1/+0",
            "", "Attack", new List<string> { "Self" }, 1, 0, 0, 0, 0, null, null, null, null, null, 
            new List<VocationClass>{ allVocations[0] }), null, null, null, null, new List<VocationClass>{ allVocations[0] })
        }));
        //79 - Trinnis, the Copycat
        allCards.Add(new CardClass("Trinnis, the Copycat", "Rare", allRaces[3], allVocations[5], 0, 0, 4, new List<EffectClass> {
            new EffectClass("Copy Status", "Copy the opponent status", "", "",
            new List<string> { "Self" }, 0, 0, 0, 0, 0, null, null, null, null, null, null),
            new EffectClass("Steal Mana", "If wins, steals 4 mana", "Mana", "Win",
            new List<string> { "Self" }, 0, 0, 4, 0, 0, null, null, null, null, null, null)
        }));

        //save all in the file
        GameClass.current = new GameClass(allVocations, allRaces, allCards, allHeroes);

        //SAVE FILE
        SaveLoadClass.Save();
    }

    //save the player collection
    private void SaveCollection()
    {
        //all cards in the collection (80)
        List<CardClass> allCardsCollection = new List<CardClass>();
        //all heroes in the collection (8)
        List<HeroClass> allHeroesCollection = new List<HeroClass>();

        for (int i = 0; i < 80; i++)
        {
            allCardsCollection.Add(new CardClass(allCards[i].GetName(), allCards[i].GetRarity(), allCards[i].GetRace(), allCards[i].GetVocation(),
                allCards[i].GetAttack(), allCards[i].GetDefense(), allCards[i].GetCost(), allCards[i].GetEffects()));
        }

        for (int i = 0; i < 8; i++)
        {
            allHeroesCollection.Add(new HeroClass(allHeroes[i].GetName(), allHeroes[i].GetRace(), allHeroes[i].GetVocation(),
                allHeroes[i].GetEffect()));
        }

        //save all in the file
        CollectionClass thisStuff = new CollectionClass(allHeroesCollection);
        foreach(CardClass cardi in allCardsCollection)
        {
            thisStuff.AddCard(cardi);
        }
        CollectionClass.current = thisStuff;

        //SAVE FILE
        SaveLoadClass.SaveCollection();
    }

    //save the player deck
    private void SaveDeck()
    {
        //all cards in the deck (12)
        List<int> allCardsDeck = new List<int>();

        for(int i = 0; i < 12; i++)
        {
            allCardsDeck.Add(i);
        }

        //save all in the file
        DeckClass.current = new DeckClass(0, allCardsDeck, true);

        //hero (first for default)
        DeckClass.current.heroIndex = 0;

        //SAVE FILE
        SaveLoadClass.SaveDeck();
    }

    //save the player deck
    private void SaveIADeck()
    {
        //basic Orc deck
        //all cards in the deck (12)
        List<int> allCardsDeck = new List<int>();

        for (int i = 0; i < 12; i++)
        {
            allCardsDeck.Add(i);
        }

        //save all in the file
        DeckClass.current = new DeckClass(0, allCardsDeck, true);

        //hero (first for default)
        DeckClass.current.heroIndex = 0;

        //SAVE FILE
        SaveLoadClass.SaveIADeck();
    }

    //getter
    public GameObject GetPlayerHand()
    {
        return playerHand;
    }
}
