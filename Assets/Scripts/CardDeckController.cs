﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardDeckController : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    //big copy card
    private GameObject bigCard;
    //game controller
    private EditDeckController editDeckController;
    //original scale
    public Vector3 originalScale;
    //collection index
    public int collectionIndex;

    private void Awake()
    {
        //find the game controller
        editDeckController = GameObject.Find("EditDeckController").GetComponent<EditDeckController>();

        originalScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        //create a copy of this gameObject, if it still does not exists and card not played yet and game is not over
        if (bigCard == null)//&& !cardPlayed 
        {
            //copy
            bigCard = Instantiate(gameObject, GameObject.Find("Canvas").transform);

            //remove the script component
            Destroy(bigCard.GetComponent<CardDeckController>());

            //position it at the right
            bigCard.GetComponent<RectTransform>().anchoredPosition = new Vector3(600, -220, 0);
            //bigCard.transform.position = new Vector3(board.transform.position.x+3.5f, 3, board.transform.position.z+1);

            //double scale
            bigCard.transform.localScale = new Vector3(originalScale.x * 2, originalScale.y * 2, originalScale.z);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Destroy(bigCard);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //if already in the deck, remove it
        if (editDeckController.playerDeck.Contains(collectionIndex))
        {
            //remove
            editDeckController.RemoveCardFromDeck(collectionIndex);
        }//else, insert it
        else
        {
            //insert
            editDeckController.AddCardInDeck(collectionIndex);
        }
    }

    private void OnMouseExit()
    {
        //if big copy exists
        if (bigCard != null)
        {
            //hide the border
            transform.Find("RedBorder").GetComponent<MeshRenderer>().enabled = false;

            //deletes it
            Destroy(bigCard);
        }
    }
}
