﻿using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

//class to save and load file info
public static class SaveLoadClass {
    //store all game information which are in the bin files
    public static List<GameClass> gameInfo = new List<GameClass>();
    //store the collection information
    public static List<CollectionClass> playerCollection = new List<CollectionClass>();
    //store the deck information
    public static List<DeckClass> playerDeck = new List<DeckClass>();
    //store the IA deck information
    public static List<DeckClass> IADeck = new List<DeckClass>();

    //save game info
    //it's static so we can call it from anywhere
    public static void Save()
    {
        gameInfo.Add(GameClass.current);
        BinaryFormatter bf = new BinaryFormatter();
        //Debug.Log(Application.persistentDataPath);
        //Debug.Break();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        //FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.database"); //you can call it anything you want
        FileStream file = File.Create(Application.dataPath + "/Database/gameInfo.database"); //you can call it anything you want
        bf.Serialize(file, gameInfo);
        file.Close();
    }

    //load game info
    public static List<GameClass> Load()
    {
        if (File.Exists(Application.dataPath + "/Database/gameInfo.database"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Open(Application.persistentDataPath + "/gameInfo.database", FileMode.Open);
            FileStream file = File.Open(Application.dataPath + "/Database/gameInfo.database", FileMode.Open);
            gameInfo = (List<GameClass>)bf.Deserialize(file);
            //Debug.Log(gameInfo[0].units.Count);
            //Debug.Log(gameInfo[0].weapons[0].GetWeight());
            file.Close();
            return gameInfo;
        }else
        {
            return null;
        }
    }

    //save deck info
    //it's static so we can call it from anywhere
    public static void SaveDeck()
    {
        string path = Application.dataPath + "/Database/playerDeck.database";

        //delete the old version
        // Delete the file if it exists.
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        //clear player deck
        playerDeck.Clear();

        playerDeck.Add(DeckClass.current);
        BinaryFormatter bf = new BinaryFormatter();
        //Debug.Log(Application.persistentDataPath);
        //Debug.Break();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        //FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.database"); //you can call it anything you want
        FileStream file = File.Create(path); //you can call it anything you want
        bf.Serialize(file, playerDeck);
        file.Close();
    }

    //load deck info
    public static List<DeckClass> LoadDeck()
    {
        if (File.Exists(Application.dataPath + "/Database/playerDeck.database"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Open(Application.persistentDataPath + "/gameInfo.database", FileMode.Open);
            FileStream file = File.Open(Application.dataPath + "/Database/playerDeck.database", FileMode.Open);
            playerDeck = (List<DeckClass>)bf.Deserialize(file);
            //Debug.Log(gameInfo[0].units.Count);
            //Debug.Log(gameInfo[0].weapons[0].GetWeight());
            file.Close();
            return playerDeck;
        }
        else
        {
            return null;
        }
    }

    //save deck info
    //it's static so we can call it from anywhere
    public static void SaveIADeck()
    {
        string path = Application.dataPath + "/Database/IADeck.database";

        //delete the old version
        // Delete the file if it exists.
        if (File.Exists(path))
        {
            File.Delete(path);
        }

        //clear player deck
        IADeck.Clear();

        IADeck.Add(DeckClass.current);
        BinaryFormatter bf = new BinaryFormatter();
        //Debug.Log(Application.persistentDataPath);
        //Debug.Break();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        //FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.database"); //you can call it anything you want
        FileStream file = File.Create(path); //you can call it anything you want
        bf.Serialize(file, IADeck);
        file.Close();
    }

    //load deck info
    public static List<DeckClass> LoadIADeck()
    {
        if (File.Exists(Application.dataPath + "/Database/IADeck.database"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Open(Application.persistentDataPath + "/gameInfo.database", FileMode.Open);
            FileStream file = File.Open(Application.dataPath + "/Database/IADeck.database", FileMode.Open);
            IADeck = (List<DeckClass>)bf.Deserialize(file);
            //Debug.Log(gameInfo[0].units.Count);
            //Debug.Log(gameInfo[0].weapons[0].GetWeight());
            file.Close();
            return IADeck;
        }
        else
        {
            return null;
        }
    }

    //save collection info
    //it's static so we can call it from anywhere
    public static void SaveCollection()
    {
        playerCollection.Add(CollectionClass.current);
        BinaryFormatter bf = new BinaryFormatter();
        //Debug.Log(Application.persistentDataPath);
        //Debug.Break();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        //FileStream file = File.Create(Application.persistentDataPath + "/gameInfo.database"); //you can call it anything you want
        FileStream file = File.Create(Application.dataPath + "/Database/playerCollection.database"); //you can call it anything you want
        bf.Serialize(file, playerCollection);
        file.Close();
    }

    //load collection info
    public static List<CollectionClass> LoadCollection()
    {
        if (File.Exists(Application.dataPath + "/Database/playerCollection.database"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            //FileStream file = File.Open(Application.persistentDataPath + "/gameInfo.database", FileMode.Open);
            FileStream file = File.Open(Application.dataPath + "/Database/playerCollection.database", FileMode.Open);
            playerCollection = (List<CollectionClass>)bf.Deserialize(file);
            //Debug.Log(gameInfo[0].units.Count);
            //Debug.Log(gameInfo[0].weapons[0].GetWeight());
            file.Close();
            return playerCollection;
        }
        else
        {
            return null;
        }
    }
}
