﻿using System;
using System.Collections.Generic;

//class to store player deck, in the game and in the bin files
[System.Serializable]
public class CollectionClass
{
    //current GameClass instance
    public static CollectionClass current;
    //hero
    public List<HeroClass> heroes;
    //cards
    [Serializable]
    public struct CardsCollection
    {
        public CardClass card;
        public int qnt;

        public CardsCollection(CardClass newCard, int newQnt = 1)
        {
            card = newCard;
            qnt = newQnt;
        }
    }
    public List<CardsCollection> cards;

    public CollectionClass()
    {
        cards = new List<CardsCollection>();
    }

    public CollectionClass(List<HeroClass> newHero)
    {
        heroes = newHero;
        cards = new List<CardsCollection>();
    }

    public CollectionClass(List<HeroClass> newHero, List<CardsCollection> newCards)
    {
        heroes = newHero;
        cards = newCards;
    }

    //add new card to the collection
    public void AddCard(CardClass newCard, int newQnt = 1)
    {
        cards.Add(new CardsCollection(newCard, newQnt));
    }

    //get only the cards in collection (without quantity)
    public List<CardClass> GetCardsCollection()
    {
        List<CardClass> cardsCollection = new List<CardClass>();
        foreach(CardsCollection oneCard in cards)
        {
            cardsCollection.Add(oneCard.card);
        }
        return cardsCollection;
    }
}
