﻿using System.Collections.Generic;

//class to store all game information which are in the bin files
[System.Serializable]
public class GameClass {
    //current GameClass instance
    public static GameClass current;
    //all possible vocations
    public List<VocationClass> allVocations;
    //all possible races
    public List<RaceClass> allRaces;
    //all possible cards
    public List<CardClass> allCards;
    //all possible heroes
    public List<HeroClass> allHeroes;

    public GameClass()
    {
        allVocations = new List<VocationClass>();
        allRaces = new List<RaceClass>();
        allCards = new List<CardClass>();
        allHeroes = new List<HeroClass>();
    }

    public GameClass(List<VocationClass> newVocations, List<RaceClass> newRaces, List<CardClass> newCards, List<HeroClass> newHeroes)
    {
        allVocations = newVocations;
        allRaces = newRaces;
        allCards = newCards;
        allHeroes = newHeroes;
    }
}