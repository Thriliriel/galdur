﻿//class for all vocations

/*
all vocations:
0 - Warrior
1 - Mage
2 - Barbarian
3 - Archer
4 - Guardian
5 - Ladin
6 - Healer
*/

[System.Serializable]
public class VocationClass
{
    //constructs
    public VocationClass()
    {
    }

    public VocationClass(string newName)
    {
        name = newName;
    }

    //name
    private string name;

    //Getters and Setters
    public string GetName()
    {
        return name;
    }
    public void SetName(string value)
    {
        name = value;
    }
}
