﻿//class for all races

/*
all races:
0 - Orcs
1 - Dwarfs
2 - Elves 
3 - Humans
*/

[System.Serializable]
public class RaceClass {
    //constructs
    public RaceClass()
    {
    }

    public RaceClass(string newName)
    {
        name = newName;
    }

    //name
    private string name;

    //Getters and Setters
    public string GetName()
    {
        return name;
    }
    public void SetName(string value)
    {
        name = value;
    }
}
